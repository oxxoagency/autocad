import axios from "axios";

const TOKEN_KEY = "authToken";

export const createToken = () => {
  // set Auth TOKEN to local storage
  // env prod
  // const url = `https://cors-anywhere.herokuapp.com/http://175.41.160.70:8181/oauth/token`;
  const url = `https://autocadelbas.oxxo.co.id/oauth/token`;

  const credentials = {
    username: "autocad@mail.com",
    password: "rahasia",
    grant_type: "password",
    client_id: "2",
    client_secret: "WABXKLbVCY6de4KV7r2HogD0ALNfCWs34jxX5BaU",
    provider: "userdata",
  };

  const headers = {
    "Content-Type": "application/json",
  };

  axios
    .post(url, credentials, {
      headers: headers,
    })
    .then((res) => {
      window.localStorage.setItem(TOKEN_KEY, res.data.access_token);
    });
};

export const createTokenChat = () => {
  // set Auth TOKEN for chat to local storage
  // env prod
  // const url = `https://cors-anywhere.herokuapp.com/http://175.41.160.70:8000/oauth/token`;
  const url = `https://apichat.oxxo.co.id/oauth/token`;

  const credentials = {
    username: "chatbot@mail.com",
    password: "rahasia",
    grant_type: "password",
    client_id: "2",
    client_secret: "sKiUp8baxayV2AoQOdShwto2KyJrPlTQyOr6M3WX",
    provider: "userdata",
  };

  const headers = {
    "Content-Type": "application/json",
  };

  axios
    .post(url, credentials, {
      headers: headers,
    })
    .then((res) => {
      window.localStorage.setItem("authTokenChat", res.data.access_token);
    });
};

export const getToken = () => {
  localStorage.getItem(TOKEN_KEY);
};

export const destroyToken = () => {
  localStorage.removeItem(TOKEN_KEY);
};

export const isLogin = () => {
  if (localStorage.getItem("key") !== "1") {
    return true;
  }
  return false;
};
