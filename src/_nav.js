export default {
  items: [
    {
      name: "Lobby",
      url: "/lobby",
      icon: "cui-home",
    },
    {
      name: "Materi",
      url: "/materi",
      icon: "cui-bookmark",
    },
    {
      name: "Logout",
      url: "/login",
      icon: "cui-lock-locked",
    },
  ],
};
