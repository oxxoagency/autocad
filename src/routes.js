import React from "react";

const Dashboard = React.lazy(() => import("./views/Dashboard"));
const Materi = React.lazy(() => import("./views/Materi"));
const Webinar = React.lazy(() => import("./views/Webinar"));
const WebinarRoom1 = React.lazy(() => import("./views/WebinarRoom1"));
const WebinarRoom2 = React.lazy(() => import("./views/WebinarRoom2"));

const routes = [
  { path: "/lobby", name: "Lobby", component: Dashboard },
  { path: "/materi/autocadday2020", name: "Materi", component: Materi },

  {
    path: "/room",
    exact: true,
    name: "Webinar",
    component: Webinar,
  },
  { path: "/room/main", name: "Main Room", component: Webinar },
  { path: "/room/1", name: "Room 1", component: WebinarRoom1 },
  { path: "/room/2", name: "Room 2", component: WebinarRoom2 },
];

export default routes;
