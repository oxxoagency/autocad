import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.scss";

import PublicRoute from "./PublicRoute";

const loading = () => (
  <div className="animated fadeIn pt-3 text-center text-white">Loading...</div>
);

// Containers
const DefaultLayout = React.lazy(() => import("./containers/DefaultLayout"));

// Pages
const HomePage = React.lazy(() => import("./views/Pages/HomePage"));
const ArticlesPage = React.lazy(() => import("./views/Pages/ArticlesPage"));
const EventsPage = React.lazy(() => import("./views/Pages/EventsPage"));
const ProductsPage = React.lazy(() => import("./views/Pages/ProductsPage"));
const SupportPage = React.lazy(() => import("./views/Pages/SupportPage"));
const Login = React.lazy(() => import("./views/Pages/Login"));
const SuccessPage = React.lazy(() => import("./views/Pages/SuccessPage"));
const ContactUs = React.lazy(() => import("./views/Pages/ContactUsPage"));
const Register = React.lazy(() => import("./views/Pages/Register"));
const Page404 = React.lazy(() => import("./views/Pages/Page404"));
const Page500 = React.lazy(() => import("./views/Pages/Page500"));

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Suspense fallback={loading()}>
          <Switch>
            <Route
              exact
              path="/"
              name="Home Page"
              render={props => <HomePage {...props} />}
            />
            <Route
              exact
              path="/articles/general"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />
            <Route
              exact
              path="/articles/general/1/1"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />
            <Route
              exact
              path="/articles/general/1/2"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />{" "}
            <Route
              exact
              path="/articles/general/1/3"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />{" "}
            <Route
              exact
              path="/articles/general/1/4"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />{" "}
            <Route
              exact
              path="/articles/general/1/5"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />
            <Route
              exact
              path="/articles/installation_and_activation/1"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />
            <Route
              exact
              path="/articles/installation_and_activation/2"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />
            <Route
              exact
              path="/articles/installation_and_activation/3"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />
            <Route
              exact
              path="/articles/transition_to_name_user"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />
            <Route
              exact
              path="/articles/new_features_2021"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />
            <Route
              exact
              path="/articles/about_autocad_including_specialized_toolset_2021"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />
            <Route
              exact
              path="/articles/autocad_infographic"
              name="Article Page"
              render={props => <ArticlesPage {...props} />}
            />
            <Route
              exact
              path="/events"
              name="Events Page"
              render={props => <EventsPage {...props} />}
            />
            <Route
              exact
              path="/products/autocad_including_specialized_toolset_2021"
              name="Products Architecture Page"
              render={props => <ProductsPage {...props} />}
            />
            <Route
              exact
              path="/products/open_product"
              name="Products Architecture Page"
              render={props => <ProductsPage {...props} />}
            />
            <Route
              exact
              path="/products/autocad_including_specialized_toolset_2021/architecture_toolset"
              name="Products Architecture Page"
              render={props => <ProductsPage {...props} />}
            />
            <Route
              exact
              path="/products/autocad_including_specialized_toolset_2021/electrical_toolset"
              name="Products Electrical Page"
              render={props => <ProductsPage {...props} />}
            />
            <Route
              exact
              path="/products/autocad_including_specialized_toolset_2021/mechanical_toolset"
              name="Products Mechanical Page"
              render={props => <ProductsPage {...props} />}
            />
            <Route
              exact
              path="/products/autocad_including_specialized_toolset_2021/map3d_toolset"
              name="Products Map 3D Page"
              render={props => <ProductsPage {...props} />}
            />
            <Route
              exact
              path="/products/autocad_including_specialized_toolset_2021/mep_toolset"
              name="Products Mep Page"
              render={props => <ProductsPage {...props} />}
            />
            <Route
              exact
              path="/products/autocad_including_specialized_toolset_2021/plant3d_toolset"
              name="Products Plant 3D Page"
              render={props => <ProductsPage {...props} />}
            />
            <Route
              exact
              path="/products/autocad_including_specialized_toolset_2021/rasterdesign_toolset"
              name="Products Raster Design Page"
              render={props => <ProductsPage {...props} />}
            />
            <Route
              exact
              path="/support"
              name="Support Page"
              render={props => <SupportPage {...props} />}
            />
            <PublicRoute
              restricted={false}
              exact
              path="/materi"
              name="Login Page"
              component={Login}
            />
            <Route
              exact
              path="/autocad2021/:source"
              name="ContactUs Page"
              render={props => <ContactUs {...props} />}
            />
            <PublicRoute
              restricted={true}
              exact
              path="/register"
              name="Register Page"
              render={props => <Register {...props} />}
            />
            <PublicRoute
              restricted={true}
              exact
              path="/success"
              name="Success Page"
              component={SuccessPage}
            />
            <Route
              exact
              path="/404"
              name="Page 404"
              render={props => <Page404 {...props} />}
            />
            <Route
              exact
              path="/500"
              name="Page 500"
              render={props => <Page500 {...props} />}
            />
            <PublicRoute
              restricted={true}
              path="/"
              name="Home"
              component={DefaultLayout}
            />
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
