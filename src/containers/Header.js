import React, { Component } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";

import styled from "styled-components";

import Logo from "./../assets/img/brand/logo_smi_vad.png";

const NavbarCustom = styled(Navbar)`
  @media screen and (min-width: 601px) {
    padding-top: 30px;
    padding-left: 100px;
    padding-right: 100px;
  }

  @media screen and (max-width: 600px) {
    padding: 40px;
  }
`;

const NavbarBrandCustom = styled(NavbarBrand)`
  @media screen and (min-width: 601px) {
    width: 30vw;
  }

  @media screen and (max-width: 600px) {
    width: 60vw;
  }
`;

const NavLinkCustom = styled(NavLink)`
  @media screen and (min-width: 601px) {
    margin-right: 40px;
    font-size: 20px;
  }

  @media screen and (max-width: 600px) {
    font-size: 15px;
  }
`;

const NavLinkWithoutMarginRight = styled(NavLink)`
  @media screen and (min-width: 601px) {
    font-size: 20px;
  }

  @media screen and (max-width: 600px) {
    font-size: 15px;
  }
`;

class HeaderCustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      dropdownOpen: false
    };
  }

  toggle = () => this.setState({ isOpen: !this.state.isOpen });

  render() {
    const { isOpen } = this.state;
    const location = this.props.location;

    return (
      <React.Fragment>
        <style>
          {`.navbar-light .navbar-nav .active  > .nav-link {
            color: red;
          }
          .navbar-light .navbar-nav .nav-link {
            color: black;
          }
          .navbar-light .navbar-nav .nav-link:hover, .navbar-light .navbar-nav .nav-link:focus {
            color: red;
          }
          @media screen and (min-width: 1186px) {
            .hvr-underline-from-center::before {
              background: red;
            }
          }
          
          @media screen and (min-width: 370px) and (max-width: 1185px) {
            .hvr-underline-from-center::before {
              background: transparent;
            }
          }
          .navbar-toggler {
            background-color: white;
          }
          `}
        </style>
        <NavbarCustom light expand="xl" className="sticky-inner">
          <NavbarBrandCustom href="/">
            <img
              src={Logo}
              alt="logo_smi"
              style={{ width: "80%", height: "auto" }}
              className="bounce-left hvr-bob"
            />
          </NavbarBrandCustom>
          <NavbarToggler onClick={this.toggle} className="ml-auto" />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto" navbar>
              {/* <NavItem active={location === "/" ? true : false}>
                <NavLinkCustom href="/">Halaman Utama</NavLinkCustom>
              </NavItem> */}
              <NavItem
                active={
                  location === "/articles/installation_and_activation/1" ||
                  location === "/articles/installation_and_activation/2" ||
                  location === "/articles/installation_and_activation/3" ||
                  location === "/articles/new_features_2021" ||
                  location ===
                    "/articles/about_autocad_including_specialized_toolset_2021" ||
                  location === "/articles/transition_to_name_user" ||
                  location === "/articles/general"
                    ? true
                    : false
                }
              >
                <NavLinkCustom
                  href="/articles/general"
                  className="hvr-underline-from-center"
                >
                  Articles
                </NavLinkCustom>
              </NavItem>
              <NavItem
                active={
                  location ===
                    "/products/autocad_including_specialized_toolset_2021" ||
                  location === "/products/open_product" ||
                  location ===
                    "/products/autocad_including_specialized_toolset_2021/architecture_toolset" ||
                  location ===
                    "/products/autocad_including_specialized_toolset_2021/electrical_toolset" ||
                  location ===
                    "/products/autocad_including_specialized_toolset_2021/mechanical_toolset" ||
                  location ===
                    "/products/autocad_including_specialized_toolset_2021/map3d_toolset" ||
                  location ===
                    "/products/autocad_including_specialized_toolset_2021/mep_toolset" ||
                  location ===
                    "/products/autocad_including_specialized_toolset_2021/plant3d_toolset" ||
                  location ===
                    "/products/autocad_including_specialized_toolset_2021/rasterdesign_toolset"
                    ? true
                    : false
                }
              >
                <NavLinkCustom
                  href="/products/autocad_including_specialized_toolset_2021"
                  className="hvr-underline-from-center"
                >
                  Products
                </NavLinkCustom>
              </NavItem>
              <NavItem active={location === "/events" ? true : false}>
                <NavLinkCustom
                  href="/events"
                  className="hvr-underline-from-center"
                >
                  Events
                </NavLinkCustom>
              </NavItem>
              <NavItem active={location === "/support" ? true : false}>
                <NavLinkWithoutMarginRight
                  href="/support"
                  className="hvr-underline-from-center"
                >
                  Support
                </NavLinkWithoutMarginRight>
              </NavItem>
            </Nav>
          </Collapse>
        </NavbarCustom>
      </React.Fragment>
    );
  }
}

export default HeaderCustom;
