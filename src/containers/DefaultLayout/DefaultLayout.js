import React, { Component, Suspense } from "react";
import { Redirect, Switch } from "react-router-dom";
import PublicRoute from "../../PublicRoute";
import { Container } from "reactstrap";

import { AppFooter, AppHeader } from "@coreui/react";

// routes config
import routes from "../../routes";

const DefaultFooter = React.lazy(() => import("./DefaultFooter"));
const DefaultHeader = React.lazy(() => import("./DefaultHeader"));

class DefaultLayout extends Component {
  loading = () => (
    <div className="animated fadeIn pt-1 text-center text-white">
      Loading...
    </div>
  );

  signOut(e) {
    e.preventDefault();
    this.props.history.push("/login");
  }

  render() {
    return (
      <div className="app">
        <AppHeader
          fixed
          style={{
            // borderBottom: "none",
            height: "100px",
            marginBottom: "20px",
          }}
        >
          <Suspense fallback={this.loading()}>
            <DefaultHeader onLogout={(e) => this.signOut(e)} />
          </Suspense>
        </AppHeader>
        <div
          className="app-body"
          style={{
            background: "#fff",
            overflow: "hidden",
          }}
        >
          <main className="main">
            <div style={{ marginBottom: "35px" }} />
            <Container fluid>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <PublicRoute
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        // change true if it is restricted
                        // change false if it is not restricted
                        restricted={true}
                        // render={props => (
                        //   <route.component {...props} />
                        // )}
                        component={(props) => <route.component {...props} />}
                      />
                    ) : null;
                  })}
                  <Redirect from="/" to="/404" />
                </Switch>
              </Suspense>
            </Container>
          </main>
        </div>
        <AppFooter
          className="text-black"
          style={{
            background: "white",
            // borderTop: "none",
            fontSize: "16px",
          }}
        >
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
