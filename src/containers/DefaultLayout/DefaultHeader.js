import React, { Component } from "react";
import PropTypes from "prop-types";

import { AppNavbarBrand } from "@coreui/react";

import logo from "../../assets/img/brand/logo_smi_vad.png";

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  render() {
    return (
      <React.Fragment>
        <AppNavbarBrand
          full={{
            src: logo,
            alt: "Brand Logo",
            background: "white",
          }}
          style={{ marginLeft: "auto", marginRight: "auto", width: "50%" }}
        />
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
