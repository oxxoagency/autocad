import React, { Component } from "react";
import PropTypes from "prop-types";

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {
    return (
      <React.Fragment>
        <span>
          <a href="https://www.synnexmetrodata.com/" style={{ color: "red" }}>
            PT. Synnex Metrodata Indonesia
          </a>{" "}
          &copy; 2020
        </span>
        {/* <span className="ml-auto">
          Powered by <a href="https://oxxo.co.id">OXXO</a>
        </span> */}
      </React.Fragment>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
