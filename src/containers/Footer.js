import React, { Component } from "react";

import { AppFooter } from "@coreui/react";
import styled from "styled-components";

import FacebookLogo from "./../assets/img/facebook_icon.png";
import InstagramLogo from "./../assets/img/instagram_icon.png";
import LinkedinLogo from "./../assets/img/linkedin_icon.png";
import TwitterLogo from "./../assets/img/twitter_icon.png";

const ImgSocmed = styled.img`
  width: 5%;
  margin: 30px;
  cursor: pointer;
`;

class FooterCustom extends Component {
  redirectToFacebook = () => {
    window.location.href = "https://www.facebook.com/SynnexMetrodata";
  };

  redirectToInstagram = () => {
    window.location.href = "https://www.instagram.com/synnexmetrodata/";
  };

  redirectToLinkedIn = () => {
    window.location.href =
      "https://www.linkedin.com/company/pt-synnex-metrodata-indonesia/";
  };

  redirectToTwitter = () => {
    window.location.href = "https://twitter.com/SynnexMetrodata";
  };

  render() {
    return (
      <React.Fragment>
        <AppFooter
          style={{
            background: "#f6f5f2 ",
            borderTop: "none",
            color: "#8E8E8E",
            fontSize: "16px",
            flex: "0 0 100px",
            display: "grid",
            textAlign: "center"
          }}
        >
          <span style={{ paddingTop: "20px", color: "black" }}>
            For More information, please contact :
            <br />
            PT. Synnex Metrodata Indonesia
            <br />
            Inquiry : Autodesk@metrodata.co.id
            <br />
            Tel. (021) 29345800
          </span>
          <div style={{ padding: "0px" }}>
            <ImgSocmed
              src={FacebookLogo}
              alt="facebook"
              onClick={this.redirectToFacebook}
              className="hvr-grow"
            />
            <ImgSocmed
              src={InstagramLogo}
              alt="instagram"
              onClick={this.redirectToInstagram}
              className="hvr-grow"
            />
            <ImgSocmed
              src={LinkedinLogo}
              alt="linkedin"
              onClick={this.redirectToLinkedIn}
              className="hvr-grow"
            />
            <ImgSocmed
              src={TwitterLogo}
              alt="twitter"
              onClick={this.redirectToTwitter}
              className="hvr-grow"
            />
          </div>
          <span style={{ paddingBottom: "20px", color: "black" }}>
            PT. Synnex Metrodata Indonesia &copy; 2020
          </span>
        </AppFooter>
      </React.Fragment>
    );
  }
}

export default FooterCustom;
