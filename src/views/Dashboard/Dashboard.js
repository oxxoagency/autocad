import React, { Component } from "react";

import { Card, CardBody, Container } from "reactstrap";

import { createTokenChat } from "../../utils";

import Content_1 from "../../assets/img/AutoCAD_3.jpg";

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    createTokenChat();
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Container
          style={{
            maxWidth: "1800px",
            height: "800px",
            backgroundImage: `url(${Content_1})`,
            backgroundSize: "100% 100%",
            display: "flex",
            position: "relative",
            left: "300px",
          }}
        >
          <Card
            style={{
              height: "800px",
              width: "550px",
              background: "#0B1921",
              marginTop: "50px",
              marginBottom: "auto",
              position: "relative",
              left: "-300px",
            }}
          >
            <CardBody className="text-left">
              <div className="text-white">
                <iframe
                  title="webinar"
                  className="d-block w-100"
                  height="300"
                  src="https://www.youtube.com/embed/jNddfU53Uew?modestbranding=1&showinfo=0"
                  frameBorder="0"
                  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                ></iframe>
                <div style={{ marginTop: "25px" }} />
                <h2>Welcome to AutoCAD Day 2020</h2>
                <div style={{ marginTop: "25px" }} />
                <p style={{ color: "#8E8E8E" }}>
                  AUTOCAD DAY 2021 adalah acara tahunan dari Autodesk yang sudah
                  diselenggarakan 4 tahun ini oleh PT. Synnex Metrodata
                  Indonesia sebagai Authorize Distributor di Indonesia untuk
                  product AutoCAD & Open. Dan di tahun 2021 ini kami kembali
                  menyelenggarakan secara Virtual ditengah pandemic Covid-19
                  namun dapat lebih menjangkau pengguna software Autodesk
                  dimanapun berada. Dan dalam event ini kami ingin memberikan
                  informasi terkait informasi teknologi terbaru dari License
                  AutoCAD yang sudah release di versi 2021. Jika ada informasi
                  lebih lanjut yang anda butuhkan, silahkan hubungi kami :
                  E-mail : Autodesk@metrodata.co.id | Tel. (021) 29345800 Ext.
                  6253
                </p>
                <div style={{ marginTop: "25px" }} />
              </div>
            </CardBody>
          </Card>
        </Container>
      </div>
    );
  }
}

export default Dashboard;
