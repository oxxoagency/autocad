import React, { Component } from "react";

import axios from "axios";

import { Card, CardBody, Container, Row, Col, Input, Button } from "reactstrap";

import { isMobile } from "react-device-detect";

import styled from "styled-components";

import { createTokenChat } from "../../utils";
import { CircleToBlockLoading } from "react-loadingg";

const ContainerCustom = styled(Container)`
  max-width: 1800px;
`;

const BubleChat = styled(Card)`
  border: none;
  margin-bottom: 10px;
`;

const BubleChatBody = styled(CardBody)`
  min-height: 100px;
  border-radius: 10px;
  background-color: #29d;
  color: #ffffff;
  margin-bottom: 10px;
`;

class WebinarRoom1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messageList: [],
      messages: "",
      sender: "",
      loader: true,
    };
  }

  componentDidMount() {
    // API call for endpoints
    createTokenChat();
    this.getMessageList();
  }

  getMessageList = () => {
    // get Message List Main Room
    const authToken = window.localStorage.getItem("authTokenChat");
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${authToken}`,
    };
    const url = `https://apichat.oxxo.co.id/message/3/2/0`;
    axios
      .get(url, {
        headers: headers,
      })
      .then((res) => {
        this.setState({ messageList: res.data.data, loader: false });
      });
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
      sender: window.localStorage.getItem("userId"),
    });
  };

  handleSendMessage = (event) => {
    event.preventDefault();
    const { messages } = this.state;

    const authToken = window.localStorage.getItem("authTokenChat");

    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${authToken}`,
    };

    const url = `https://apichat.oxxo.co.id/message`;

    axios
      .post(
        url,
        {
          client: "3",
          data: {
            message: messages,
            room_id: "2",
            sender: window.localStorage.getItem("userId"),
          },
        },
        {
          headers: headers,
        }
      )
      .then((res) => {
        this.getMessageList();
      });
  };

  updateScroll() {
    const element = document.getElementById("card-chat");
    if (element !== null) {
      element.scrollTop = element.scrollHeight;
    }
    return true;
  }

  render() {
    const { messageList, messages, loader } = this.state;
    if (isMobile) return <div>This content is unavailable on mobile</div>;
    return (
      <div className="animated fadeIn">
        <ContainerCustom>
          <Row>
            <Col style={{ paddingRight: "0" }}>
              <iframe
                title="webinar"
                className="d-block w-100"
                height="600"
                src="https://www.youtube-nocookie.com/embed/LhdM28r8iGo?modestbranding=1&showinfo=0"
                frameBorder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              ></iframe>
              <div style={{ marginTop: "10px" }} />
              <Card style={{ height: "190px", background: "#0B1921" }}>
                <CardBody>
                  <Card style={{ height: "150px", marginBottom: "5px" }}>
                    <CardBody>
                      <h3>
                        AEC Breakout : AutoCAD for Architecture, Engineering &
                        Construction
                      </h3>
                    </CardBody>
                  </Card>
                </CardBody>
              </Card>
            </Col>
            <Col style={{ maxWidth: "600px" }}>
              <Card style={{ height: "800px", background: "#0B1921" }}>
                <CardBody>
                  <Card
                    id="card-chat"
                    style={{
                      height: "698px",
                      marginBottom: "5px",
                      overflowY: "scroll",
                    }}
                  >
                    <CardBody>
                      {loader ? (
                        <CircleToBlockLoading color="#20a8d8" />
                      ) : (
                        <BubleChat>
                          {messageList.map((item) => (
                            <BubleChatBody key={Math.random()}>
                              @{" "}
                              {item.sender_name !== null &&
                              item.sender_name !== ""
                                ? item.sender_name
                                : "anonymous"}
                              <div style={{ marginBottom: "5px" }} />
                              {item.message}
                            </BubleChatBody>
                          ))}
                        </BubleChat>
                      )}
                    </CardBody>
                  </Card>
                  <Card style={{ alignItems: "center" }}>
                    <CardBody style={{ padding: "10px" }}>
                      <Row>
                        <Input
                          name="messages"
                          style={{ width: "390px" }}
                          type="text"
                          value={messages}
                          onChange={this.handleChange}
                          placeholder="Ketik pesanmu disini..."
                        />
                        <Button
                          type="submit"
                          color="primary"
                          onClick={this.handleSendMessage}
                        >
                          Kirim
                        </Button>
                      </Row>
                    </CardBody>
                  </Card>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </ContainerCustom>
      </div>
    );
  }
}

export default WebinarRoom1;
