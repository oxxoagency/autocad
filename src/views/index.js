import Dashboard from "./Dashboard";
import Materi from "./Materi";
import Webinar from "./Webinar";
import {
  Login,
  Page404,
  Page500,
  Register,
  LandingPage,
  SuccessPage,
} from "./Pages";

export {
  LandingPage,
  SuccessPage,
  Page404,
  Page500,
  Register,
  Login,
  Dashboard,
  Materi,
  Webinar,
};
