import React, { Component } from "react";

import { Card, CardBody, Container, CardGroup, Button } from "reactstrap";

import styled from "styled-components";

import MateriDownload1 from "../../assets/materi_1.pdf";
import MateriDownload2 from "../../assets/materi_2.pdf";
import MateriDownload3 from "../../assets/materi_3.pdf";
import MateriDownload4 from "../../assets/materi_4.pdf";
import MateriDownload5 from "../../assets/materi_5.pdf";

const CardMateri = styled(Card)`
  height: 110px;
  margin-bottom: 10px;
  padding: 10px;
  font-size: 20px;
  cursor: pointer;
  color: black;
  align-items: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
  overflow: auto;
  // border-radius: 15px;
  :hover {
    height: 112px;
    border: 1px solid #f86c6b;
  }
`;
const CardMateriInfo = styled(Card)`
  height: 110px;
  margin-bottom: 10px;
  padding: 10px;
  font-size: 20px;
  background: transparent;
  color: black;
  border: none;
`;

const CardMainMateriSelected = styled(Card)`
  height: 710px;
  margin-bottom: 0;
  :hover {
    height: 710px;
    border: 1px solid #f86c6b;
  }
`;

const CardMateriSelected = styled(Card)`
  height: 670px;
  margin-bottom: 10px;
  padding: 30px;
  font-size: 15px;
  overflow-y: auto;
  border: none;
`;

const CardSchedule1 = (props) => {
  return (
    <CardMateri onClick={props.addTrip}>
      <h5 style={{ marginTop: "30px" }}>Transition to Named User</h5>
      <div style={{ marginBottom: "5px" }} />
      <span style={{ color: "#8E8E8E", fontSize: "16px" }}></span>
    </CardMateri>
  );
};

const CardSchedule2 = (props) => {
  return (
    <CardMateri onClick={props.addTrip}>
      <h5 style={{ marginTop: "30px" }}>
        AutoCAD 2021 new features with specialized toolsets
      </h5>
      <div style={{ marginBottom: "5px" }} />
    </CardMateri>
  );
};

const CardSchedule3 = (props) => {
  return (
    <CardMateri onClick={props.addTrip}>
      <h5 style={{ marginTop: "30px" }}>
        Software Awareness - The Genuine Autodesk Software
      </h5>
      <div style={{ marginBottom: "5px" }} />
    </CardMateri>
  );
};

const CardSchedule4 = (props) => {
  return (
    <CardMateri onClick={props.addTrip}>
      <h5 style={{ marginTop: "30px" }}>
        AutoCAD for Architecture, Engineering & Construction
      </h5>
      <div style={{ marginBottom: "5px" }} />
    </CardMateri>
  );
};

const CardSchedule5 = (props) => {
  return (
    <CardMateri onClick={props.addTrip}>
      <h5 style={{ marginTop: "30px" }}>
        AutoCAD for Design and Manufacturing
      </h5>
      <div style={{ marginBottom: "5px" }} />
    </CardMateri>
  );
};

const CardSchedule6 = (props) => {
  return (
    <CardMateriInfo onClick={props.addTrip}>
      <h4>
        For More information, please contact :
        <br />
        PT. Synnex Metrodata Indonesia
        <br />
        Inquiry : Autodesk@metrodata.co.id
        <br />
        Tel. (021) 29345800
      </h4>
      <div style={{ marginBottom: "5px" }} />
    </CardMateriInfo>
  );
};

const CardScheduleSelected1 = (props) => {
  return (
    <CardMateriSelected onClick={props.redirectTo}>
      <h4>Transition to Named User</h4>
      <div style={{ marginBottom: "10px" }} />
      Speaker : Supardi Pondok, Channel Manager, Autodesk Indonesia
      <div style={{ marginBottom: "20px" }} />
      Description : Anda lebih dari sekadar nomor seri untuk kami. Itulah
      sebabnya kami menghentikan rencana kami berdasarkan nomor seri dan
      meluncurkan rencana baru berdasarkan orang. Mulai 7 Agustus 2020, Anda
      dapat menukarkan license Maintenance Plan anda atau License Multi-user
      Subscription dengan biaya yang sesuai dengan apa yang Anda bayar hari ini.
      <div style={{ marginBottom: "20px" }} />
      <iframe
        title="AutoCAD 2021"
        className="d-block w-100"
        width="350"
        height="350"
        src="https://www.youtube.com/embed/6HFf3OONEqU?modestbranding=1&showinfo=0"
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
      <Button href={MateriDownload1} color="danger" download>
        Download Materi
      </Button>
    </CardMateriSelected>
  );
};

const CardScheduleSelected2 = (props) => {
  return (
    <CardMateriSelected onClick={props.redirectTo}>
      <h3>AutoCAD 2021 new features with specialized toolsets</h3>
      <div style={{ marginBottom: "20px" }} />
      Speaker : Ahmad Topan, Technical Specialist - Synnex Metrodata Indonesia
      <div style={{ marginBottom: "20px" }} />
      Description : Bekerja lebih cerdas akan lebih mudah dengan AutoCAD 2021
      Perangkat lunak AutoCAD® 2021 sudah mencakup industry-specific toolsets;
      alur kerja yang ditingkatkan di desktop, web, dan seluler; dan fitur baru
      seperti drawing history. Subscription dengan biaya yang sesuai dengan apa
      yang Anda bayar hari ini.
      <div style={{ marginBottom: "20px" }} />
      <iframe
        title="AutoCAD 2021"
        className="d-block w-100"
        width="350"
        height="350"
        src="https://www.youtube.com/embed/okcPyJQePZM?modestbranding=1&showinfo=0"
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
      <Button href={MateriDownload2} color="danger" download>
        Download Materi
      </Button>
    </CardMateriSelected>
  );
};

const CardScheduleSelected3 = (props) => {
  return (
    <CardMateriSelected onClick={props.redirectTo}>
      <h3>Software Awareness - The Genuine Autodesk Software</h3>
      <div style={{ marginBottom: "20px" }} />
      Speaker : Marieska Andries, Genuine Software Initiative - Autodesk
      Indonesia
      <div style={{ marginBottom: "20px" }} />
      Description : Meningkatkan performa kerja dan menjaga keamanan hasil
      design, dengan software Autodesk asli.
      <div style={{ marginBottom: "20px" }} />
      <iframe
        title="AutoCAD 2021"
        className="d-block w-100"
        width="350"
        height="350"
        src="https://www.youtube.com/embed/E9Hv0Beif0c?modestbranding=1&showinfo=0"
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
      <Button href={MateriDownload3} color="danger" download>
        Download Materi
      </Button>
    </CardMateriSelected>
  );
};

const CardScheduleSelected4 = (props) => {
  return (
    <React.Fragment>
      <CardMateriSelected onClick={props.redirectToRoom1}>
        <h4>
          AEC Breakout : AutoCAD for Architecture, Engineering & Construction
        </h4>
        <div style={{ marginBottom: "20px" }} />
        Speaker : Ahmad Topan, Technical Specialist - Synnex Metrodata Indonesia
        <div style={{ marginBottom: "20px" }} />
        Description : AutoCAD 2021 untuk desain "Architecture" dan "Mechanical
        Electrical Plumbing (MEP)", yang membantu anda bekerja lebih cepat,
        meningkatkan produktivitas anda, dan memungkinkan anda bekerja dari mana
        saja, kapan saja.
        <div style={{ marginBottom: "20px" }} />
        <iframe
          title="AutoCAD 2021"
          className="d-block w-100"
          width="350"
          height="350"
          src="https://www.youtube.com/embed/Tke5U1d7c64?modestbranding=1&showinfo=0"
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        ></iframe>
        <Button href={MateriDownload4} color="danger" download>
          Download Materi
        </Button>
      </CardMateriSelected>
    </React.Fragment>
  );
};

const CardScheduleSelected5 = (props) => {
  return (
    <CardMateriSelected onClick={props.redirectTo}>
      <h3>D&M Breakout : AutoCAD for Design and Manufacturing</h3>
      <div style={{ marginBottom: "20px" }} />
      Speaker : Ahmad Topan, Technical Specialist - Synnex Metrodata Indonesia
      <div style={{ marginBottom: "20px" }} />
      Description : AutoCAD 2021 untuk desain "mechanical engineering" dan
      "electrical engineering", yang membantu anda bekerja lebih cepat,
      meningkatkan produktivitas anda, dan memungkinkan anda bekerja dari mana
      saja, kapan saja.
      <div style={{ marginBottom: "20px" }} />
      <iframe
        title="AutoCAD 2021"
        className="d-block w-100"
        width="350"
        height="350"
        src="https://www.youtube.com/embed/xpGg5bD1BHI?modestbranding=1&showinfo=0"
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      ></iframe>
      <Button href={MateriDownload5} color="danger" download>
        Download Materi
      </Button>
    </CardMateriSelected>
  );
};

class Materi extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      emptyState: true,
      schedule: "",
    };
  }

  triggerVisibility1 = () => {
    this.setState({
      ...this.state,
      visible: true,
      emptyState: false,
      schedule: "1",
    });
  };

  triggerVisibility2 = () => {
    this.setState({
      ...this.state,
      visible: true,
      emptyState: false,
      schedule: "2",
    });
  };

  triggerVisibility3 = () => {
    this.setState({
      ...this.state,
      visible: true,
      emptyState: false,
      schedule: "3",
    });
  };

  triggerVisibility4 = () => {
    this.setState({
      ...this.state,
      visible: true,
      emptyState: false,
      schedule: "4",
    });
  };

  triggerVisibility5 = () => {
    this.setState({
      ...this.state,
      visible: true,
      emptyState: false,
      schedule: "5",
    });
  };

  triggerVisibility6 = () => {
    this.setState({
      ...this.state,
      visible: true,
      emptyState: false,
      schedule: "6",
    });
  };

  // redirectToWebinar = () => {
  //   this.props.history.push("/room/main");
  // };

  render() {
    const { visible, emptyState, schedule } = this.state;

    return (
      <div className="animated fadeIn">
        <Container style={{ maxWidth: "auto", paddingTop: "20px" }}>
          <Card style={{ maxHeight: "auto", border: "none" }}>
            <CardBody>
              <CardGroup>
                <Card style={{ border: "none" }}>
                  <CardBody style={{ overflowY: "auto" }}>
                    <CardSchedule1 addTrip={this.triggerVisibility1} />
                    <CardSchedule2 addTrip={this.triggerVisibility2} />
                    <CardSchedule3 addTrip={this.triggerVisibility3} />
                    <CardSchedule4 addTrip={this.triggerVisibility5} />
                    <CardSchedule5 addTrip={this.triggerVisibility6} />
                    <CardSchedule6 />
                  </CardBody>
                </Card>
                <Card style={{ border: "none" }}>
                  <CardBody
                    style={{
                      boxShadow:
                        "0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24)",
                    }}
                  >
                    <CardMainMateriSelected>
                      {emptyState && (
                        <CardBody
                          style={{ alignSelf: "center", marginTop: "320px" }}
                        >
                          Silakan pilih materi terlebih dahulu
                        </CardBody>
                      )}
                      <CardBody>
                        {visible && schedule === "1" && (
                          <CardScheduleSelected1
                          // redirectTo={this.redirectToWebinar}
                          />
                        )}
                        {visible && schedule === "2" && (
                          <CardScheduleSelected2 />
                        )}
                        {visible && schedule === "3" && (
                          <CardScheduleSelected3 />
                        )}
                        {visible && schedule === "5" && (
                          <CardScheduleSelected4 />
                        )}
                        {visible && schedule === "6" && (
                          <CardScheduleSelected5 />
                        )}
                      </CardBody>
                    </CardMainMateriSelected>
                  </CardBody>
                </Card>
              </CardGroup>
            </CardBody>
          </Card>
        </Container>
      </div>
    );
  }
}

export default Materi;
