import React from "react";

import { Container, Card, Media } from "reactstrap";

import styled from "styled-components";

const ContainerCustom = styled(Container)`
  padding: 50px !important;
`;

const CardCustomVideo = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 50px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const Mep = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustomVideo>
          <iframe
            title="AutoCAD 2021"
            className="d-block w-100"
            height="500"
            src="https://www.youtube.com/embed/dzehcMDdltA?modestbranding=1&showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </CardCustomVideo>
        <CardCustom>
          <Media>
            <Media body>
              <Media heading>
                MEP (Mechanical, Electrical, Plumbing) Toolset
              </Media>
              <ul>
                <li>
                  membantu menyusun, merancang, dan membangun system managemen
                  dokumen yang lebih efisien dengan lebih dari 10.500 komponen
                  mekanikal, kelistrikan, dan pipa saluran air untuk gedung.
                  Sangat mudah dalam membuat desain “ducting”, kelistrikan,
                  pekerjaan saluran gedung, dan semua desain kelengkapan MEP
                  pada suatu gedung.
                </li>
              </ul>
            </Media>
          </Media>
        </CardCustom>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default Mep;
