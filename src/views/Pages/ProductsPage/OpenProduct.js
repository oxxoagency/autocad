import React from "react";
import { Container, Card, Media, Button } from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

import SampleImageProduct1 from "../../../assets/img/Open_Product1.png";
import SampleImageProduct2 from "../../../assets/img/Open_Product2.png";
import SampleImageProduct3 from "../../../assets/img/Open_Product3.png";
import SampleImageProduct4 from "../../../assets/img/Open_Product4.png";
import SampleImageProduct5 from "../../../assets/img/Open_Product5.png";
import SampleImageProduct6 from "../../../assets/img/Open_Product6.png";

const ContainerCustom = styled(Container)`
  @media screen and (min-width: 601px) {
    padding: 50px !important;
  }

  @media screen and (max-width: 600px) {
    padding: 0px !important;
  }
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 0px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const OpenProduct = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <img
            src={SampleImageProduct1}
            alt="image_product"
            style={{ width: "100%" }}
          ></img>
          <Media style={{ marginTop: "50px" }}>
            <Media body>
              Kami juga melayani "open product autodesk" untuk desain dalam
              bidang manufaktur, asitektur, sipil, konstruksi, dan media
              entertainment.
              <Media heading style={{ marginTop: "30px" }}>
                AutoCAD Limited Technology (LT)
              </Media>{" "}
              <p>
                Software AutoCAD LT 2021 adalah Computer Aided Design (CAD) yang
                diandalkan oleh arsitek, insinyur, professional konstruksi, dan
                semua orang yang bergerak dibidang teknik untuk menghasilkan
                dokumentasi gambar kerja dua dimensi (2D). Membuat gambar 2D
                yang presisi dan lebih cepat dengan alat draf yang mudah
                digunakan di AutoCAD LT sehingga mampu meningkatkan
                produktivitas dengan membuat otomatisasi objek gambar dengan
                tugas-tugas umum dan mampu menyederhanakan alur kerja.
                Memudahkan dalam membuat dokumentasi gambar 2D dengan teknologi
                perangkat alat menggambar dalam mengedit, membuat desain, dan
                memberikan anotasi pada objek gambar.
              </p>
              <p>
                AutoCAD LT memilki interface yang intuitif dengan fitur yang
                mudah dipahami mencakup ribbon tabs, multifunctional grips,
                customizable tool palettes, dan perintah-perintah yang mudah
                digunakan. AutoCAD LT saat ini memiliki benefit tambahan yaitu
                autoCAD Web dan autoCAD Mobile Apps dimana aplikasi tersebut
                saling terhubung satu dengan yang lainnya sehingga memungkinkan
                untuk bekerja dimana saja dan kapan saja. Menggunakan autoCAD
                Mobile Apps sangatlah mudah karena interface-nya sama dengan
                autoCAD versi desktop. Cukup dengan mendownload dan install pada
                smart phone kemudian “sign in” menggunakan autodesk ID. Begitu
                juga autoCAD versi Web cukup dengan menggunakan browser dan
                “sign in” pada link web.autocad.com dengan benefit autoCAD Web
                kita bisa mengedit gambar kerja tanpa harus install aplikasi
                autoCAD.
              </p>
              <p>Beberapa Fitur AutoCAD LT :</p>
              <ul>
                <li>Drawing history</li>
                <li>Xref Compare</li>
                <li>Blocks Pallete</li>
                <li>AutoCAD on any device</li>
                <li>Cloud storage connectivity</li>
                <li>Quick measure</li>
                <li>Enhanced DWG compare</li>
                <li>Purge redesign</li>
                <li>AutoCAD anytime, anywhere</li>
                <li>Save to web and mobile</li>
                <li>Shared views</li>
                <li>Comprehensive 2D documentation</li>
                <li>Innovative technologies</li>
              </ul>
              <img
                src={SampleImageProduct2}
                alt="image_product"
                style={{ width: "100%" }}
              ></img>
              <Media heading style={{ marginTop: "30px" }}>
                Inventor Limited Technology (LT)
              </Media>{" "}
              <p>
                Software Inventor LT bertujuan memperkenalkan Computer Aided
                Design (CAD) untuk membuat gambar “mechanical” tiga dimensi (3D)
                yang terintegrasi berdasarkan gambar dua dimensi (2D). Inventor
                LT mampu membuat model 3D parametrik yang sangat baik, bisa
                kolaborasi secara online diperangkat apa pun, interoperabilitas
                multiCAD, dan mampu menghasilkan gambar DWG yang sesuai standar
                industri.
              </p>
              <p>Beberapa Fitur Inventor LT :</p>
              <ul>
                <li>Model-based definition</li>
                <li>Free-form tools</li>
                <li>3D CAD data import/export</li>
                <li>DWG interoperability</li>
                <li>Parametric modeling</li>
                <li>Shared views</li>
              </ul>
              <img
                src={SampleImageProduct3}
                alt="image_product"
                style={{ width: "100%" }}
              ></img>
              <Media heading style={{ marginTop: "30px" }}>
                Revit Limited Technology (LT)
              </Media>{" "}
              <p>
                Revit LT adalah software BIM (Building Information Modeling)
                yang menghasilkan desain dan dokumentasi arsitektur tiga dimensi
                (3D) berkualitas tinggi. Revit LT mendukung alur kerja BIM yang
                dapat memvisualisasikan dan mengkomunikasikan desain yang dibuat
                dengan lebih jelas untuk akurasi dan efisiensi proyek yang lebih
                baik.
              </p>
              <p>
                Revit LT memiliki kemampuan parametrik untuk membuat model 3D,
                parametrik ini berhubungan dengan semua elemen dalam proyek
                gambr sehingga memungkinkan koordinasi dan manajemen perubahan
                yang sangat cepat dan terintegrasi pada semua elemen 3D model.
                Fitur ini sudah disediakan oleh revit tanpa harus
                mengkonfigurasi secara manual.
              </p>
              <p>Beberapa Fitur Revit LT :</p>
              <ul>
                <li>3D design and visualization</li>
                <li>3D BIM</li>
                <li>High-quality documentation</li>
                <li>Autogenerated schedules</li>
                <li>Help verify material quantities</li>
                <li>Photorealistic rendering in the cloud</li>
              </ul>
              <img
                src={SampleImageProduct4}
                alt="image_product"
                style={{ width: "100%" }}
              ></img>
              <Media heading style={{ marginTop: "30px" }}>
                Maya Limited Technology (LT)
              </Media>{" "}
              <p>
                Maya LT adalah software animasi dengan teknologi tinggi untuk
                membuat animasi dari karakter, property, dan environment
                tertentu agar terlihat lebih realistis. Maya LT digunakan juga
                untuk pengembangan game 3D mencakup fitur animasi, rigging,
                pemodelan, dan pencahayaan intuitif agar pembuat game mampu
                bekerja lebih cepat dan tanpa batasan kreatif.
              </p>
              <p>Beberapa Fitur Mayat LT :</p>
              <ul>
                <li>3D modeling tools</li>
                <li>Revamped UV editor</li>
                <li>Shaders and materials</li>
                <li>Built-in sculpting tools</li>
                <li>LOD tools for game model efficiency</li>
                <li>Physically based shader materials</li>
                <li>Lighting and texture baking</li>
                <li>Modeling improvements</li>
                <li>3D animation tools</li>
                <li>Rigging tools</li>
                <li>Shape editor</li>
                <li>Time editor</li>
                <li>Graph editor</li>
              </ul>
              <img
                src={SampleImageProduct5}
                alt="image_product"
                style={{ width: "100%" }}
              ></img>
              <Media heading style={{ marginTop: "30px" }}>
                Mudbox
              </Media>{" "}
              <p>
                Mudbox adalah software untuk membuat karakter dan environment
                tiga dimensi (3D) dengan sangat detail menggunakan teknologi
                alat digital yang intuitif berdasarkan sculpt yang nyata. Fitur
                utama Autodesk Mudbox, tampilan karakter yang tepat sasaran,
                brush-based tools menyediakan kontrol kreatif yang lebih besar,
                memungkinkan untuk bekerja dengan model secara sederhana dan
                lebih efisien. Sesuaikan alat dengan tepat ― bentuk ujung kuas
                dengan falloff curves dan stamps, atau ubah responsnya terhadap
                tekanan bahkan dapat berkolaborasi dengan user lain pada mesh
                yang sama secara bersamaan untuk meningkatkan produktivitas dan
                alur kerja sehingga Tekstur objek gambar lebih akurat dan tepat
                sasaran.
              </p>
              <p>Beberapa Fitur Mudbox :</p>
              <ul>
                <li>Digital sculpting tool</li>
                <li>Paint directly on 3D models</li>
                <li>Dynamic tessellation</li>
                <li>Advanced retopology tools</li>
                <li>Texture baking</li>
                <li>Smooth, brush-based workflows</li>
              </ul>
              <img
                src={SampleImageProduct6}
                alt="image_product"
                style={{ width: "100%" }}
              ></img>
              <Media heading style={{ marginTop: "30px" }}>
                Sketchbook
              </Media>{" "}
              <p>
                Di Autodesk, kami percaya kreativitas dimulai dengan sebuah ide.
                Dari sketsa konseptual cepat hingga karya seni yang sepenuhnya
                selesai, sketsa adalah inti dari proses kreatif. Kita tidak
                pernah tahu kapan ide hebat akan muncul, jadi akses ke alat
                sketsa kreatif yang cepat dan kuat adalah bagian tak ternilai
                dari proses kreatif apa pun.
              </p>
              <p>
                SketchBook memiliki interface yang sederhana sehingga mampu
                memaksimalkan ruang gambar setiap perangkatnya. Alat sketsa yang
                familiar di ruang digital seperti pensil, tinta, spidol, dan
                lebih dari 190 kuas yang dapat disesuaikan sehingga dapat
                menggabungkan tekstur dan bentuk. Ekstra perpustakaan Warna
                Copic® eksklusif yang disukai oleh ilustrator. Bisa juga
                menggunakan penggaris tradisional dan panduan elips untuk garis
                yang tepat. Atau garis bantu dengan 16-sektor Radial Symmetry
                dan Predictive Stroke yang menghaluskan garis dan mengoreksi
                bentuk sehingga mampu menghasilakan sketsa yang sempurna.
              </p>
              <p>Beberapa Fitur Sketchbook :</p>
              <ul>
                <li>Perspective guides on mobile</li>
                <li>Curved Ruler</li>
                <li>Natural drawing experience</li>
                <li>Unlimited brushes</li>
                <li>Work with layers naturally</li>
                <li>Four symmetry dimensions</li>
                <li>Scan sketch with mobile</li>
                <li>Copic Color Library</li>
                <li>Flipbook animation</li>
                <li>Distort transform</li>
                <li>Eighteen blending modes</li>
              </ul>
              <Media heading style={{ marginTop: "30px" }}>
                Premium Plan
              </Media>
              <p>
                <Button
                  color="link"
                  href="/support"
                  style={{
                    boxShadow: "none",
                    color: "red",
                    padding: "0",
                    marginTop: "20px"
                  }}
                >
                  Hubungi Kami
                </Button>
              </p>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default OpenProduct;
