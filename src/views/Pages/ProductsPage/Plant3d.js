import React from "react";

import { Container, Card, Media } from "reactstrap";

import styled from "styled-components";

const ContainerCustom = styled(Container)`
  padding: 50px !important;
`;

const CardCustomVideo = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 50px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const Plant3d = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustomVideo>
          <iframe
            title="AutoCAD 2021"
            className="d-block w-100"
            height="500"
            src="https://www.youtube.com/embed/dzehcMDdltA?modestbranding=1&showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </CardCustomVideo>
        <CardCustom>
          <Media>
            <Media body>
              <Media heading>Plant 3D Toolset</Media>
              <ul>
                <li>
                  mampu menghasilkan gambar P&ID secara efisien dan
                  mengintegrasikannya ke dalam model 3D “plant design”
                  menggunakan toolset khusus untuk “plant design” dan
                  fitur-fitur khusus desain pipa. Cepat dalam membuat “layout”
                  pabrik, skematik diagram, dll.
                </li>
              </ul>
            </Media>
          </Media>
        </CardCustom>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default Plant3d;
