import React from "react";

import { Container, Card, Media } from "reactstrap";

import styled from "styled-components";

// import SampleImageProduct from "../../../assets/img/sample_architec_toolset.png";

const ContainerCustom = styled(Container)`
  padding: 50px !important;
`;

const CardCustomVideo = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 50px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const Architecture = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustomVideo>
          <iframe
            title="AutoCAD 2021"
            className="d-block w-100"
            height="500"
            src="https://www.youtube.com/embed/dzehcMDdltA?modestbranding=1&showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </CardCustomVideo>
        <CardCustom>
          <Media>
            <Media body>
              <Media heading>Architecture Toolset</Media>
              <ul>
                <li>
                  mempercepat pekerjaan dalam pembuatan gambar desain dibidang
                  arsitektur karena dilengkapi dengan “library” lebih dari
                  750.000 simbol, komponen, dan detail komponen yang berhubungan
                  dengan arsitektur. Otomatis dalam pembuatan anotasi, layer,
                  dan properti suatu objek. Mudah dalam membuat detail gambar
                  dari tampilan 3D model, dan dengan cepat mengeluarkan
                  “schedules”, daftar, dan table.
                </li>
              </ul>
            </Media>
            {/* <Media right href="#">
              <Media
                object
                src={SampleImageProduct}
                alt="Generic placeholder image"
                style={{ height: "100px", width: "100px" }}
              />
            </Media> */}
          </Media>
        </CardCustom>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default Architecture;
