import React from "react";

import { Container, Card, Media } from "reactstrap";

import styled from "styled-components";

const ContainerCustom = styled(Container)`
  padding: 50px !important;
`;

const CardCustomVideo = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 50px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const Electrical = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustomVideo>
          <iframe
            title="AutoCAD 2021"
            className="d-block w-100"
            height="500"
            src="https://www.youtube.com/embed/dzehcMDdltA?modestbranding=1&showinfo=0"
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </CardCustomVideo>
        <CardCustom>
          <Media>
            <Media body>
              <Media heading>Electrical Toolset</Media>
              <ul>
                <li>
                  meningkatkan produktivitas dokumentasi gambar dan desain
                  sistem kelistrikan dengan tersedianya “content library” lebih
                  dari 65.000 simbol kelistrikan yang terinteligen sehingga
                  memudahkan untuk mengeluarkan banyaknya komponen kelistrikan,
                  banyaknya kabel, dan deskripsi dari setiap komponen secara
                  otomatis. Toolset ini dgunakan untuk membuat desain tata letak
                  panel dan skema diagram kelistrikan.
                </li>
              </ul>
            </Media>
          </Media>
        </CardCustom>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default Electrical;
