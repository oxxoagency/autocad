import React from "react";

import { Nav, NavItem, NavLink } from "reactstrap";

import styled from "styled-components";

const NavCustom = styled(Nav)`
  @media screen and (min-width: 1801px) {
    padding: 50px;
    position: fixed;
  }

  @media screen and (max-width: 1800px) {
    padding-bottom: 10px;
  }
`;

const NavLinkCustom = styled(NavLink)`
  color: black;
  :hover {
    color: red;
  }
`;

const LineBreakSideBar = styled.hr`
  width: 300px;
  float: left;
  margin-left: 20px;
`;

const SideNavProductsPage = props => {
  return (
    <React.Fragment>
      <style>
        {`.active  > .nav-link {
              color: red;
            }`}
      </style>
      <NavCustom vertical>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021">
            AutoCAD Including Specialized Toolset 2021
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={props.location === "/products/open_product" ? true : false}
        >
          <NavLinkCustom href="/products/open_product">
            Open Product
          </NavLinkCustom>
          {/* <LineBreakSideBar /> */}
        </NavItem>
        {/* <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/architecture_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/architecture_toolset">
            Architecture Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/electrical_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/electrical_toolset">
            Electrical Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/mechanical_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/mechanical_toolset">
            Mechanical Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/map3d_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/map3d_toolset">
            Map 3D Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/mep_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/mep_toolset">
            MEP Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/plant3d_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/plant3d_toolset">
            Plant 3D Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/rasterdesign_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/rasterdesign_toolset">
            Raster Design Toolset
          </NavLinkCustom>
        </NavItem> */}
      </NavCustom>
    </React.Fragment>
  );
};

export default SideNavProductsPage;
