import React, { useState, useRef, useEffect } from "react";

import styled from "styled-components";

import { Row, Col } from "reactstrap";
import Header from "./../../../containers/Header";

import SideNav from "./SideNav";
import AutoCADIncludingSpecialToolset from "./AutoCADIncludingSpecialToolset";
import OpenProduct from "./OpenProduct";
import ArchitectureComponent from "./Architecture";
import ElectricalComponent from "./Electrical";
import MechanicalComponent from "./Mechanical";
import Map3dComponent from "./Map3d";
import MepComponent from "./Mep";
import Plant3dComponent from "./Plant3d";
import RasterComponent from "./Raster";

import Footer from "./../../../containers/Footer";

const Layout = styled.div`
  background: white;
`;

const Body = styled.div`
  width: 100%;
  height: auto;
  background: white;
  margin-top: 0;
`;

const ColCustom = styled(Col)`
  @media screen and (min-width: 1233px) {
    margin-left: 100px;
  }

  @media screen and (max-width: 1232px) {
    margin-left: 0px;
  }
`;

const ProductsPage = props => {
  const location = props.location.pathname;
  const [isSticky, setSticky] = useState(false);
  const ref = useRef(null);
  const handleScroll = () => {
    if (ref.current) {
      setSticky(ref.current.getBoundingClientRect().top <= 0);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", () => handleScroll);
    };
  }, []);

  return (
    <Layout className="app">
      <Body className="app-body animated fadeIn">
        <main className="main">
          <div
            className={`sticky-wrapper${isSticky ? " sticky" : ""}`}
            ref={ref}
          >
            <Header location={location} />
          </div>
          <Row style={{ padding: "40px" }}>
            <Col lg="2">
              <SideNav location={location} />
            </Col>
            <ColCustom xl="9">
              {location ===
                "/products/autocad_including_specialized_toolset_2021" && (
                <AutoCADIncludingSpecialToolset />
              )}
              {location === "/products/open_product" && <OpenProduct />}
              {location ===
                "/products/autocad_including_specialized_toolset_2021/architecture_toolset" && (
                <ArchitectureComponent />
              )}
              {location ===
                "/products/autocad_including_specialized_toolset_2021/electrical_toolset" && (
                <ElectricalComponent />
              )}
              {location ===
                "/products/autocad_including_specialized_toolset_2021/mechanical_toolset" && (
                <MechanicalComponent />
              )}
              {location ===
                "/products/autocad_including_specialized_toolset_2021/map3d_toolset" && (
                <Map3dComponent />
              )}
              {location ===
                "/products/autocad_including_specialized_toolset_2021/mep_toolset" && (
                <MepComponent />
              )}
              {location ===
                "/products/autocad_including_specialized_toolset_2021/plant3d_toolset" && (
                <Plant3dComponent />
              )}
              {location ===
                "/products/autocad_including_specialized_toolset_2021/rasterdesign_toolset" && (
                <RasterComponent />
              )}
            </ColCustom>
          </Row>
        </main>
      </Body>
      <Footer />
    </Layout>
  );
};

export default ProductsPage;
