import React from "react";
import { Container, Card, Media, Button } from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

import SampleImageProduct from "../../../assets/img/content_product_1.jpg";

const ContainerCustom = styled(Container)`
  @media screen and (min-width: 601px) {
    padding: 0px !important;
  }

  @media screen and (max-width: 600px) {
    padding: 0px !important;
  }
`;

const CardCustomVideo = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 0px;
    overflow: auto;
    border: none;
    margin-top: 20px;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
    margin-top: 20px;
  }
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 50px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const AutoCADIncludingSpecialToolset = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <img
            src={SampleImageProduct}
            alt="image_product"
            style={{ width: "100%" }}
          ></img>
          <CardCustomVideo>
            <iframe
              title="AutoCAD 2021"
              className="d-block w-100"
              height="500"
              src="https://www.youtube.com/embed/2-yK7vyJlM0?modestbranding=1&showinfo=0"
              frameBorder="0"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            ></iframe>
          </CardCustomVideo>
          <Media style={{ marginTop: "50px" }}>
            <Media body>
              AutoCAD 2021 dirilis pada maret 2020. Software yang sudah lebih
              dari 30 tahun digunakan oleh profesional di bidang arsitektur dan
              bidang manufacture ini memiliki pembaruan besar. Setelah melakukan
              penyempurnaan selama beberapa dekade, pada versi baru ini,
              pengguna bisa mendapatkan akses ke tujuh set alat profesional
              (toolset) sekaligus yang dilengklapi dengan 750.000 fitur dengan
              fungsi yang berbeda-beda, ditambah dengan akses AutoCAD web dan
              mobile apps. Tujuh tool tersebut di antaranya Architecture,
              Mechanical, Electrical, Map 3D, MEP, Raster Design, dan Plant 3D.
              Sementara itu, disediakan ribuan fitur lainnya berfungsi untuk
              membantu pengguna dalam menyelesaikan tugas-tugas khusus, seperti
              menggambar layout panel listrik dengan cepat, bekerja dengan data
              Geographic Information System (GIS), serta mengedit gambar yang
              dipindai.
              <Media heading style={{ marginTop: "30px" }}>
                Architecture Toolset
              </Media>
              <ul style={{ marginTop: "30px" }}>
                <li>
                  Mempercepat pekerjaan dalam pembuatan gambar desain dibidang
                  arsitektur karena dilengkapi dengan “library” lebih dari
                  750.000 simbol, komponen, dan detail komponen yang berhubungan
                  dengan arsitektur. Otomatis dalam pembuatan anotasi, layer,
                  dan properti suatu objek. Mudah dalam membuat detail gambar
                  dari tampilan 3D model, dan dengan cepat mengeluarkan
                  “schedules”, daftar, dan table.
                </li>
              </ul>
              <Media heading style={{ marginTop: "30px" }}>
                Electrical Toolset
              </Media>
              <ul style={{ marginTop: "30px" }}>
                <li>
                  Meningkatkan produktivitas dokumentasi gambar dan desain
                  sistem kelistrikan dengan tersedianya “content library” lebih
                  dari 65.000 simbol kelistrikan yang terinteligen sehingga
                  memudahkan untuk mengeluarkan banyaknya komponen kelistrikan,
                  banyaknya kabel, dan deskripsi dari setiap komponen secara
                  otomatis. Toolset ini dgunakan untuk membuat desain tata letak
                  panel dan skema diagram kelistrikan.
                </li>
              </ul>
              <Media heading style={{ marginTop: "30px" }}>
                Mechanical Toolset
              </Media>
              <ul style={{ marginTop: "30px" }}>
                <li>
                  Mempercepat desain produk dengan fitur desain mekanikal dan
                  lebih dari 700.000 suku cadang dan simbol manufaktur yang
                  intelijen. Mengotomatiskan pekerjaan, seperti menghasilkan
                  komponen-komponen mesin dan mengeluarkan Bills of Material.
                </li>
              </ul>
              <Media heading style={{ marginTop: "30px" }}>
                Map 3D Toolset
              </Media>
              <ul style={{ marginTop: "30px" }}>
                <li>
                  Menggabungkan topologi GIS (Geographic Information System)
                  dengan AutoCAD sehingga Anda dapat menggunakan dan memelihara
                  data CAD dan GIS untuk perencanaan, desain, dan manajemen
                  data. Gunakan alat berbasis data managemen untuk mengelola dan
                  mengumpulkan data GIS dengan desain data perencanaan.
                  mengaktifkan skema data standar, alur kerja otomatis, dan
                  templat laporan. Templat laporan untuk Industri kelistrikan,
                  pengairan, perlimbahan, dan Gas pada suatu kota.
                </li>
              </ul>
              <Media heading style={{ marginTop: "30px" }}>
                MEP (Mechanical, Electrical, Plumbing) Toolset
              </Media>
              <ul style={{ marginTop: "30px" }}>
                <li>
                  Membantu menyusun, merancang, dan membangun system managemen
                  dokumen yang lebih efisien dengan lebih dari 10.500 komponen
                  mekanikal, kelistrikan, dan pipa saluran air untuk gedung.
                  Sangat mudah dalam membuat desain “ducting”, kelistrikan,
                  pekerjaan saluran gedung, dan semua desain kelengkapan MEP
                  pada suatu gedung.
                </li>
              </ul>
              <Media heading style={{ marginTop: "30px" }}>
                Plant 3D Toolset
              </Media>
              <ul style={{ marginTop: "30px" }}>
                <li>
                  Mampu menghasilkan gambar P&ID secara efisien dan
                  mengintegrasikannya ke dalam model 3D “plant design”
                  menggunakan toolset khusus untuk “plant design” dan
                  fitur-fitur khusus desain pipa. Cepat dalam membuat “layout”
                  pabrik, skematik diagram, dll.
                </li>
              </ul>
              <Media heading style={{ marginTop: "30px" }}>
                Raster Design Toolset
              </Media>
              <ul style={{ marginTop: "30px" }}>
                <li>
                  Mengkonversi dari gambar raster menjadi objek DWG dengan fitur
                  raster ke vector. Mudah untuk mengedit, membersihkan gambar
                  raster sambal mengubahnya menjadi gambar vector dengan
                  “interface” yang familiar karena tidak berbeda dengan versi
                  sebelumnya.
                </li>
              </ul>
              <Media heading style={{ marginTop: "30px" }}>
                AutoCAD Web & Mobile Apps
              </Media>
              <ul style={{ marginTop: "30px" }}>
                <li>
                  Membantu engineer, drafter, dan pihak-pihak yang terkait dalam
                  pembuatan gambar desain suatu projek untuk bisa bekerja sama,
                  kapan saja dan dimana saja tanpa dibatasi jarak. Fitur ini
                  membantu semua pihak yang terkait dalam suatu projek untuk
                  bisa melihat dan merevisi suatu gambar dalam waktu bersamaan
                  meskipun berbeda-beda tempat baik kota, propinsi, pulau,
                  maupun antar negara.
                </li>
              </ul>
              <Button
                color="link"
                href="/support"
                style={{
                  boxShadow: "none",
                  color: "red",
                  padding: "0",
                  marginTop: "20px"
                }}
              >
                Hubungi Kami
              </Button>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default AutoCADIncludingSpecialToolset;
