import React, { Component } from "react";

import axios from "axios";

import {
  Button,
  CardGroup,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  Input,
  Label,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from "reactstrap";
import { Link } from "react-router-dom";

import { isMobile } from "react-device-detect";

import RightContent from "../../../assets/img/AutoCAD_Sale.jpeg";
import RightContent_V2 from "../../../assets/img/AutoCAD_Flash_Sale.jpg";
import Logo_AutoCAD from "../../../assets/img/autocad_logo.jpg";
import { CircleToBlockLoading } from "react-loadingg";

class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contact_us_email: "",
      contact_us_phone: "",
      contact_us_name: "",
      contact_us_company: "",
      contact_us_title: "",
      contact_us_address: "",
      contact_us_product_inquiry_1: "",
      contact_us_product_inquiry_2: "",
      contact_us_product_inquiry_3: "",
      ischecked1: false,
      ischecked2: false,
      ischecked3: false,
      contact_us_qty: 0,
      contact_us_link: this.props.match.params.source,
      loader: false
    };
  }

  componentDidMount() {
    const url = `https://autocadelbas.oxxo.co.id/oauth/token`;

    const credentials = {
      username: "autocad@mail.com",
      password: "rahasia",
      grant_type: "password",
      client_id: "2",
      client_secret: "WABXKLbVCY6de4KV7r2HogD0ALNfCWs34jxX5BaU",
      provider: "userdata"
    };

    const headers = {
      "Content-Type": "application/json"
    };

    axios
      .post(url, credentials, {
        headers: headers
      })
      .then(res => {
        window.localStorage.setItem("authToken", res.data.access_token);
      });
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleChangeCheckBox1 = event => {
    this.setState({
      [event.target.name]: event.target.checked
    });
    if (event.target.checked === true) {
      this.setState({
        contact_us_product_inquiry_1: "1"
      });
    } else {
      this.setState({
        contact_us_product_inquiry_1: ""
      });
    }
  };

  handleChangeCheckBox2 = event => {
    this.setState({
      [event.target.name]: event.target.checked
    });
    if (event.target.checked === true) {
      this.setState({
        contact_us_product_inquiry_2: "2"
      });
    } else {
      this.setState({
        contact_us_product_inquiry_2: ""
      });
    }
  };

  handleChangeCheckBox3 = event => {
    this.setState({
      [event.target.name]: event.target.checked
    });
    if (event.target.checked === true) {
      this.setState({
        contact_us_product_inquiry_3: "3"
      });
    } else {
      this.setState({
        contact_us_product_inquiry_3: ""
      });
    }
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ loader: true });

    const {
      contact_us_email,
      contact_us_phone,
      contact_us_name,
      contact_us_company,
      contact_us_title,
      contact_us_address,
      contact_us_product_inquiry_1,
      contact_us_product_inquiry_2,
      contact_us_product_inquiry_3,
      contact_us_qty,
      contact_us_link
    } = this.state;
    const authToken = window.localStorage.getItem("authToken");

    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${authToken}`
    };

    const url = `https://autocadelbas.oxxo.co.id/contact-us`;

    axios
      .post(
        url,
        {
          contact_us_email,
          contact_us_phone,
          contact_us_name,
          contact_us_company,
          contact_us_title,
          contact_us_address,
          contact_us_product_inquiry_1,
          contact_us_product_inquiry_2,
          contact_us_product_inquiry_3,
          contact_us_qty,
          contact_us_link
        },
        {
          headers: headers
        }
      )
      .then(res => {
        this.setState({ loader: false });
        if (res.data.status === 200) {
          alert(
            "Thank You For Contacting Us. We will be in touch with you as soon as possible"
          );
          this.props.history.push("/");
        } else {
          alert("Harap lengkapi data yang dibutuhkan");
        }
      });
  };

  render() {
    const {
      contact_us_email,
      contact_us_phone,
      contact_us_name,
      contact_us_company,
      contact_us_title,
      contact_us_address,
      ischecked1,
      ischecked2,
      ischecked3,
      contact_us_qty,
      contact_us_link
    } = this.state;
    if (isMobile)
      return (
        <div className="app align-items-center">
          <Card>
            <CardBody>
              <Form>
                <img
                  src={RightContent}
                  style={{ width: "325px", height: "325px" }}
                  alt="logo"
                />
                <p className="text-muted">
                  Syarat & Ketentuan :
                  <br />
                  1. Silakan lengkapi data berikut untuk pembelian
                  <br />
                  2. Semua kolom wajib diisi
                  <br />
                  3. Mohon gunakan email yang valid untuk keperluan verifikasi
                </p>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="icon-user"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="contact_us_name"
                    type="text"
                    placeholder="Fullname"
                    autoComplete="fullname"
                    required
                    value={contact_us_name}
                    onChange={this.handleChange}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>@</InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="contact_us_email"
                    type="email"
                    placeholder="Email"
                    autoComplete="email"
                    required
                    value={contact_us_email}
                    onChange={this.handleChange}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="icon-home"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="contact_us_company"
                    type="text"
                    placeholder="Company/Agency/Campus Name"
                    autoComplete="user_company"
                    required
                    value={contact_us_company}
                    onChange={this.handleChange}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="icon-briefcase"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="contact_us_title"
                    type="text"
                    placeholder="Title"
                    required
                    value={contact_us_title}
                    onChange={this.handleChange}
                  ></Input>
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="icon-home"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="contact_us_address"
                    type="textarea"
                    placeholder="Company/Agency/Campus Address"
                    autoComplete="user_company_address"
                    required
                    value={contact_us_address}
                    onChange={this.handleChange}
                    style={{ resize: "none" }}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="icon-phone"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="contact_us_phone"
                    type="text"
                    placeholder="Your Phone Number"
                    autoComplete="user_phone_number"
                    required
                    value={contact_us_phone}
                    onChange={this.handleChange}
                  />
                </InputGroup>
                <InputGroup>
                  <Label for="product_inquiry" sm={3}>
                    Product Inquiry
                  </Label>
                  <Col>
                    <Label>
                      <Input
                        name="ischecked1"
                        type="checkbox"
                        checked={ischecked1}
                        onChange={this.handleChangeCheckBox1}
                      />{" "}
                      AutoCAD Toolset
                    </Label>
                  </Col>
                  <Col>
                    <Label>
                      <Input
                        name="ischecked2"
                        type="checkbox"
                        checked={ischecked2}
                        onChange={this.handleChangeCheckBox2}
                      />{" "}
                      AutoCAD LT
                    </Label>
                  </Col>
                  <Col>
                    <Label>
                      <Input
                        name="ischecked3"
                        type="checkbox"
                        checked={ischecked3}
                        onChange={this.handleChangeCheckBox3}
                      />{" "}
                      Revit LT Suite
                    </Label>
                  </Col>
                </InputGroup>
                <Label>Quantity</Label>
                <InputGroup className="mb-3">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="icon-key"></i>
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input
                    name="contact_us_qty"
                    type="number"
                    placeholder="Quantity"
                    autoComplete="product_quantity"
                    value={contact_us_qty}
                    min={0}
                    step="1"
                    onChange={this.handleChange}
                  />
                </InputGroup>
                <Input
                  name="contact_us_link"
                  type="text"
                  value={contact_us_link}
                  onChange={this.handleChange}
                  style={{ display: "none" }}
                />
                <div style={{ marginTop: "30px" }}></div>
                <Link to="/lobby">
                  <Button
                    type="submit"
                    onClick={this.handleSubmit}
                    block
                    style={{ background: "#f10007", color: "white" }}
                  >
                    Contact Us Now
                  </Button>
                </Link>
              </Form>
            </CardBody>
          </Card>
        </div>
      );
    if (this.state.loader) {
      return <CircleToBlockLoading color="#20a8d8" />;
    } else {
      return (
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="12">
                <CardGroup>
                  <Card>
                    <CardBody className="p-4">
                      <Form>
                        <Row>
                          <Col md="8">
                            <h3 style={{ color: "#f10007" }}>
                              AutoCAD 2021 Flash Sale!
                            </h3>
                          </Col>
                          <img
                            src={Logo_AutoCAD}
                            style={{ width: "150px", height: "40px" }}
                            alt="logo"
                          />
                        </Row>
                        <p className="text-muted">
                          Syarat & Ketentuan :
                          <br />
                          1. Silakan lengkapi data berikut untuk pembelian
                          <br />
                          2. Semua kolom wajib diisi
                          <br />
                          3. Mohon gunakan email yang valid untuk keperluan
                          verifikasi
                        </p>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="contact_us_name"
                            type="text"
                            placeholder="Fullname"
                            autoComplete="fullname"
                            required
                            value={contact_us_name}
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>@</InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="contact_us_email"
                            type="email"
                            placeholder="Email"
                            autoComplete="email"
                            required
                            value={contact_us_email}
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-home"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="contact_us_company"
                            type="text"
                            placeholder="Company/Agency/Campus Name"
                            autoComplete="user_company"
                            required
                            value={contact_us_company}
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-briefcase"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="contact_us_title"
                            type="text"
                            placeholder="Title"
                            required
                            value={contact_us_title}
                            onChange={this.handleChange}
                          ></Input>
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-home"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="contact_us_address"
                            type="textarea"
                            placeholder="Company/Agency/Campus Address"
                            autoComplete="user_company_address"
                            required
                            value={contact_us_address}
                            onChange={this.handleChange}
                            style={{ resize: "none" }}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-phone"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="contact_us_phone"
                            type="text"
                            placeholder="Your Phone Number"
                            autoComplete="user_phone_number"
                            required
                            value={contact_us_phone}
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup>
                          <Label for="product_inquiry" sm={3}>
                            Product Inquiry
                          </Label>
                          <Col>
                            <Label>
                              <Input
                                name="ischecked1"
                                type="checkbox"
                                checked={ischecked1}
                                onChange={this.handleChangeCheckBox1}
                              />{" "}
                              AutoCAD Toolset
                            </Label>
                          </Col>
                          <Col>
                            <Label>
                              <Input
                                name="ischecked2"
                                type="checkbox"
                                checked={ischecked2}
                                onChange={this.handleChangeCheckBox2}
                              />{" "}
                              AutoCAD LT
                            </Label>
                          </Col>
                          <Col>
                            <Label>
                              <Input
                                name="ischecked3"
                                type="checkbox"
                                checked={ischecked3}
                                onChange={this.handleChangeCheckBox3}
                              />{" "}
                              Revit LT Suite
                            </Label>
                          </Col>
                        </InputGroup>
                        <Label>Quantity</Label>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-key"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="contact_us_qty"
                            type="number"
                            placeholder="Quantity"
                            autoComplete="product_quantity"
                            value={contact_us_qty}
                            min={0}
                            step="1"
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <Input
                          name="contact_us_link"
                          type="text"
                          value={contact_us_link}
                          onChange={this.handleChange}
                          style={{ display: "none" }}
                        />
                        <div style={{ marginTop: "30px" }}></div>
                        <Link to="/lobby">
                          <Button
                            type="submit"
                            onClick={this.handleSubmit}
                            block
                            style={{ background: "#f10007", color: "white" }}
                          >
                            Contact Us Now
                          </Button>
                        </Link>
                      </Form>
                    </CardBody>
                  </Card>
                  <Card>
                    <CardBody
                      className="text-center"
                      style={{ display: "flex", alignItems: "center" }}
                    >
                      <div>
                        <img
                          src={RightContent_V2}
                          className="d-md-down-none"
                          style={{ height: "700px", width: "500px" }}
                          alt="schedule"
                        />
                      </div>
                    </CardBody>
                  </Card>
                </CardGroup>
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
  }
}

export default ContactUs;
