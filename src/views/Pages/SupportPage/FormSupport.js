import React, { Component } from "react";

import axios from "axios";

import styled from "styled-components";

import {
  Container,
  Col,
  Row,
  Button,
  Form,
  FormGroup,
  Label,
  Input
} from "reactstrap";

import { CircleToBlockLoading } from "react-loadingg";

const ContainerCustom = styled(Container)`
  background: white;
  padding: 80px !important;
  margin-left: 0;
`;

const FormCustom = styled(Form)`
  @media screen and (min-width: 601px) {
    width: 60%;
  }

  @media screen and (max-width: 600px) {
    width: 100%;
  }
`;

const InputCustom = styled(Input)`
  height: 60px;
  :focus {
    border-color: red;
    box-shadow: none;
  }
`;

class FormSupport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      support_name: "",
      support_email: "",
      support_company: "",
      support_phone: "",
      support_jabatan: "",
      support_subjek: "",
      support_serial_number: "",
      support_desc: "",
      support_tujuan_1: "",
      support_tujuan_2: "",
      ischecked1: false,
      ischecked2: false,
      loader: false
    };
  }

  componentDidMount() {
    const url = `https://autocadelbas.oxxo.co.id/oauth/token`;

    const credentials = {
      username: "autocad@mail.com",
      password: "rahasia",
      grant_type: "password",
      client_id: "2",
      client_secret: "WABXKLbVCY6de4KV7r2HogD0ALNfCWs34jxX5BaU",
      provider: "userdata"
    };

    const headers = {
      "Content-Type": "application/json"
    };

    axios
      .post(url, credentials, {
        headers: headers
      })
      .then(res => {
        window.localStorage.setItem("authToken", res.data.access_token);
      });
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  handleChangeCheckBox1 = event => {
    this.setState({
      [event.target.name]: event.target.checked
    });
    if (event.target.checked === true) {
      this.setState({
        support_tujuan_1: "1"
      });
    } else {
      this.setState({
        support_tujuan_1: ""
      });
    }
  };

  handleChangeCheckBox2 = event => {
    this.setState({
      [event.target.name]: event.target.checked
    });
    if (event.target.checked === true) {
      this.setState({
        support_tujuan_2: "2"
      });
    } else {
      this.setState({
        support_tujuan_2: ""
      });
    }
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ loader: true });

    const {
      support_name,
      support_email,
      support_company,
      support_phone,
      support_jabatan,
      support_subjek,
      support_serial_number,
      support_desc,
      support_tujuan_1,
      support_tujuan_2
    } = this.state;
    const authToken = window.localStorage.getItem("authToken");

    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${authToken}`
    };

    // const url = `http://175.41.160.70:8181/support`;
    const url = `https://autocad.oxxo.co.id/support`;

    axios
      .post(
        url,
        {
          support_name,
          support_email,
          support_company,
          support_phone,
          support_jabatan,
          support_subjek,
          support_serial_number,
          support_desc,
          support_tujuan_1,
          support_tujuan_2
        },
        {
          headers: headers
        }
      )
      .then(res => {
        this.setState({ loader: false });
        console.log(res.data);
        if (res.data.status === 200) {
          alert(
            "Terima Kasih Sudah Menghubungi Kami, Kami Akan Segera Merespon Anda"
          );
          this.props.history.push("/");
        } else {
          alert("Harap lengkapi data yang dibutuhkan");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    const {
      support_name,
      support_email,
      support_company,
      support_phone,
      support_jabatan,
      support_subjek,
      support_serial_number,
      support_desc,
      ischecked1,
      ischecked2,
      loader
    } = this.state;
    if (loader) {
      return <CircleToBlockLoading color="#f64846" />;
    } else {
      return (
        <ContainerCustom fluid>
          <Row className="justify-content-center">
            <FormCustom>
              <FormGroup>
                <Label
                  for="contactinfo"
                  style={{ fontSize: "25px", fontWeight: "600" }}
                >
                  Hubungi Kami
                </Label>
              </FormGroup>
              <FormGroup>
                <InputCustom
                  type="text"
                  name="support_name"
                  id="username"
                  placeholder="Nama"
                  value={support_name}
                  onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup>
                <InputCustom
                  type="email"
                  name="support_email"
                  id="useremail"
                  placeholder="Email"
                  value={support_email}
                  onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup>
                <InputCustom
                  type="text"
                  name="support_company"
                  id="companyname"
                  placeholder="Nama Perusahaan"
                  value={support_company}
                  onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup>
                <InputCustom
                  type="text"
                  name="support_phone"
                  id="userphone"
                  placeholder="No Telepon"
                  value={support_phone}
                  onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup>
                <InputCustom
                  type="text"
                  name="support_jabatan"
                  id="usertitle"
                  placeholder="Jabatan"
                  value={support_jabatan}
                  onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup>
                <InputCustom
                  type="text"
                  name="support_serial_number"
                  id="serialnumber"
                  placeholder="Serial Number"
                  value={support_serial_number}
                  onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup>
                <InputCustom
                  type="text"
                  name="support_subjek"
                  id="subject"
                  placeholder="Subjek"
                  value={support_subjek}
                  onChange={this.handleChange}
                />
              </FormGroup>
              <FormGroup>
                <InputCustom
                  type="textarea"
                  name="support_desc"
                  id="description"
                  placeholder="Deskripsi"
                  value={support_desc}
                  onChange={this.handleChange}
                  style={{ height: "100px", resize: "none" }}
                />
              </FormGroup>
              <FormGroup style={{ marginTop: "50px" }}>
                <Label
                  for="purpose"
                  style={{ fontSize: "25px", fontWeight: "600" }}
                >
                  Tujuan
                </Label>
              </FormGroup>
              <Row>
                <Col md={2}>
                  <FormGroup check>
                    <Input
                      type="checkbox"
                      name="ischecked1"
                      id="technicalproduct"
                      checked={ischecked1}
                      onChange={this.handleChangeCheckBox1}
                    />
                    <Label for="technicalproduct" check>
                      Teknikal Produk
                    </Label>
                  </FormGroup>
                </Col>
                <Col md={2}>
                  <FormGroup check>
                    <Input
                      type="checkbox"
                      name="ischecked2"
                      id="marketingproduct"
                      checked={ischecked2}
                      onChange={this.handleChangeCheckBox2}
                    />
                    <Label for="marketingproduct" check>
                      Marketing Produk
                    </Label>
                  </FormGroup>
                </Col>
              </Row>
              <Button
                style={{
                  marginTop: "20px",
                  background: "#F10007",
                  color: "white",
                  width: "100px"
                }}
                size="lg"
                onClick={this.handleSubmit}
              >
                Kirim
              </Button>
            </FormCustom>
          </Row>
        </ContainerCustom>
      );
    }
  }
}

export default FormSupport;
