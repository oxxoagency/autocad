import React from "react";

import styled from "styled-components";

import Header from "./../../../containers/Header";

import FormSupport from "./FormSupport";

import Footer from "./../../../containers/Footer";

const Layout = styled.div`
  background: white;
`;

const Body = styled.div`
  width: 100%;
  height: auto;
  background: white;
  margin-top: 0;
`;

const SupportPage = props => {
  return (
    <Layout className="app">
      <Body className="app-body animated fadeIn">
        <main className="main">
          <Header location={props.location.pathname} />
          <FormSupport history={props.history} />
        </main>
      </Body>
      <Footer />
    </Layout>
  );
};

export default SupportPage;
