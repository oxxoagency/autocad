import React, { Component } from "react";

import { Card, CardBody, Col, Container, Row } from "reactstrap";

import Background from "../../../assets/img/AutoCAD_1.jpg";
import { destroyToken } from "../../../utils";

const sectionStyle = {
  width: "100%",
  height: "auto",
  background: `url(${Background}) no-repeat center fixed`,
  backgroundSize: "100% 100%",
  paddingTop: "10px",
  paddingLeft: "10rem",
};

class SuccessPage extends Component {
  componentDidMount() {
    // destroy Auth TOKEN if exist
    destroyToken();
  }

  render() {
    return (
      <div className="app flex-row align-items-center" style={sectionStyle}>
        <Container>
          <Row>
            <Col md="12">
              <Card
                className="text-white py-5"
                style={{
                  height: "250px",
                  float: "right",
                  background: "red",
                  opacity: "80%",
                }}
              >
                <CardBody className="text-center">
                  <div>
                    <h2>Thanks For Register!</h2>
                    <p>
                      Terima kasih telah mendaftar di webinar AutoCAD Day 2020
                    </p>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default SuccessPage;
