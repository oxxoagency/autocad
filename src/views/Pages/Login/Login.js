import React, { Component } from "react";

import axios from "axios";

import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";

import { createToken } from "../../../utils";

import LogoSynnex from "../../../assets/img/logo_synnex.png";
import LogoAutoDesk from "../../../assets/img/logo_autodesk_vad.png";
import { CircleToBlockLoading } from "react-loadingg";

import styled from "styled-components";

const InputCustom = styled(Input)`
  :focus {
    border-color: red;
    box-shadow: none;
  }
`;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      loader: false,
    };
  }

  componentDidMount() {
    //API call for Create auth token
    createToken();
    window.localStorage.setItem("key", "0");
  }

  componentDidUpdate() {
    window.localStorage.setItem("key", "1");
  }

  handleChangeEmail = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmitEmail = (event) => {
    event.preventDefault();
    this.setState({ loader: true });

    const { email } = this.state;
    const authToken = window.localStorage.getItem("authToken");

    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${authToken}`,
    };

    const url = `https://autocadelbas.oxxo.co.id/registered`;

    axios
      .post(
        url,
        { email },
        {
          headers: headers,
        }
      )
      .then((res) => {
        this.setState({ loader: false });
        if (res.data.status === 200 && email !== "") {
          alert("Akun terdaftar, Selamat Datang");
          window.localStorage.setItem("userId", res.data.data.user_id);
          window.localStorage.setItem(
            "userFullName",
            res.data.data.user_fullname
          );
          this.props.history.push("/materi/autocadday2020");
        } else if (res.data.status === 404 && email !== "") {
          alert(res.data.message);
        } else {
          alert("It is empty email");
        }
      });
  };

  render() {
    const { email } = this.state;
    if (this.state.loader) {
      return <CircleToBlockLoading color="#f64846" />;
    } else {
      return (
        <div className="app flex-row align-items-center bg-white">
          <Container>
            <Row className="justify-content-center">
              <Col md="5">
                <Card
                  className="p-4"
                  style={{
                    boxShadow:
                      "0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24)",
                    borderRadius: "15px",
                  }}
                >
                  <CardBody>
                    <div style={{ textAlign: "center" }}>
                      <img
                        src={LogoSynnex}
                        alt="logo"
                        style={{ width: "80%", height: "auto" }}
                      />
                      <img
                        src={LogoAutoDesk}
                        alt="logo"
                        style={{ width: "80%", height: "auto" }}
                      />
                    </div>
                    <Form>
                      <p className="text-muted">
                        Silahkan Masukkan Email Yang Sudah Terdaftar
                      </p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-envelope"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <InputCustom
                          name="email"
                          type="email"
                          placeholder="Email"
                          autoComplete="email"
                          required
                          value={email}
                          onChange={this.handleChangeEmail}
                        />
                      </InputGroup>
                      <Button
                        type="submit"
                        color="danger"
                        block
                        onClick={this.handleSubmitEmail}
                      >
                        Login
                      </Button>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
  }
}

export default Login;
