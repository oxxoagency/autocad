import React, { Component } from "react";

import { Link } from "react-router-dom";
import { Card, CardBody, Col, Container, Row } from "reactstrap";

import Background from "../../../assets/img/AutoCAD_Sale.jpeg";
import { destroyToken } from "../../../utils";

const sectionStyle = {
  width: "100%",
  height: "auto",
  background: `url(${Background}) no-repeat center fixed`,
  backgroundSize: "100% 100%",
};

class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      source: "web",
    };
  }
  componentDidMount() {
    // destroy Auth TOKEN if exist
    destroyToken();
  }

  render() {
    const { source } = this.state;
    const getSource = `/autocad2021/:source=${source}`;
    return (
      <div className="app flex-row align-items-center" style={sectionStyle}>
        <Container>
          <Row>
            <Col md="8">
              <Link to={getSource}>
                <Card
                  style={{
                    height: "100px",
                    width: "300px",
                    background: "#f10007",
                  }}
                >
                  <CardBody
                    className="text-center"
                    style={{ paddingTop: "30px" }}
                  >
                    <div>
                      <h2 style={{ color: "white" }}>CONTACT US NOW</h2>
                    </div>
                  </CardBody>
                </Card>
              </Link>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default LandingPage;
