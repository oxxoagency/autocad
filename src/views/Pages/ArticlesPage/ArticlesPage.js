import React, { useState, useRef, useEffect } from "react";

import styled from "styled-components";

import { Row, Col } from "reactstrap";
import Header from "./../../../containers/Header";

import SideNav from "./SideNav";

import ArticleGeneral from "./General";
import ArticleGeneral1 from "./ArticleGeneral1";
import ArticleGeneral2 from "./ArticleGeneral2";
import ArticleGeneral3 from "./ArticleGeneral3";
import ArticleGeneral4 from "./ArticleGeneral4";
import ArticleGeneral5 from "./ArticleGeneral5";
import ArticleInstallation1 from "./InstallationActivation1";
import ArticleInstallation2 from "./InstallationActivation2";
import ArticleInstallation3 from "./InstallationActivation3";
import TransitionToNamedUser from "./TransitionToNamedUser";
import InfoGraphicComponent from "./Infographic";
import AboutAutoCAD from "./AboutAutoCAD";
import AboutAutoCADSpecialized from "./AboutAutoCADIncludingSpecializedToolset";

import Footer from "./../../../containers/Footer";

const Layout = styled.div`
  background: white;
`;

const Body = styled.div`
  width: 100%;
  height: auto;
  background: white;
  margin-top: 0;
`;

const ColCustom = styled(Col)`
  @media screen and (min-width: 1233px) {
    margin-left: 100px;
  }

  @media screen and (max-width: 1232px) {
    margin-left: 0px;
  }
`;

const ArticlesPage = props => {
  const location = props.location.pathname;
  const [isSticky, setSticky] = useState(false);
  const ref = useRef(null);
  const handleScroll = () => {
    if (ref.current) {
      setSticky(ref.current.getBoundingClientRect().top <= 0);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", () => handleScroll);
    };
  }, []);

  return (
    <Layout className="app">
      <Body className="app-body animated fadeIn">
        <main className="main">
          <div
            className={`sticky-wrapper${isSticky ? " sticky" : ""}`}
            ref={ref}
          >
            <Header location={location} />
          </div>
          <Row style={{ padding: "40px" }}>
            <Col lg="2">
              <SideNav location={location} />
            </Col>
            <ColCustom xl="9">
              {location === "/articles/general" && <ArticleGeneral />}
              {location === "/articles/general/1/1" && <ArticleGeneral1 />}
              {location === "/articles/general/1/2" && <ArticleGeneral2 />}
              {location === "/articles/general/1/3" && <ArticleGeneral3 />}
              {location === "/articles/general/1/4" && <ArticleGeneral4 />}
              {location === "/articles/general/1/5" && <ArticleGeneral5 />}
              {location === "/articles/installation_and_activation/1" && (
                <ArticleInstallation1 />
              )}
              {location === "/articles/installation_and_activation/2" && (
                <ArticleInstallation2 />
              )}
              {location === "/articles/installation_and_activation/3" && (
                <ArticleInstallation3 />
              )}
              {location === "/articles/autocad_infographic" && (
                <InfoGraphicComponent />
              )}
              {location === "/articles/transition_to_name_user" && (
                <TransitionToNamedUser />
              )}
              {location === "/articles/new_features_2021" && <AboutAutoCAD />}
              {location ===
                "/articles/about_autocad_including_specialized_toolset_2021" && (
                <AboutAutoCADSpecialized />
              )}
            </ColCustom>
          </Row>
        </main>
      </Body>
      <Footer />
    </Layout>
  );
};

export default ArticlesPage;
