import React from "react";
import { Container, Card, Media, Button } from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

import Image1 from "../../../assets/img/content_general_2_1.png";

const ContainerCustom = styled(Container)`
  padding: 0px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 40px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const ImgWrapper = styled.div`
  padding: 0px;
  text-align: center;
`;

const ImgCustom = styled.img`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const ArticleGeneral2 = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Media>
            <Media body>
              <ImgWrapper>
                <ImgCustom
                  src={Image1}
                  alt="image_1"
                  style={{ width: "100%" }}
                ></ImgCustom>
              </ImgWrapper>
              <Media heading>
                Technology Developments and Easier Room Design
              </Media>
              <p>
                In order to make a building, in the initial view, people who
                tend to be traditional will think of calling a builder. Next,
                the builder will immediately try to visualize what he thinks
                about the building design by drawing it on a paper. However,
                from this, there is a very basic weakness, namely that no one
                understands what the image means other than the builder who has
                visualized the image. With the advancement of technology in the
                world, nowadays a software that can create images with various
                size details has emerged.
              </p>
              <p>
                In addition, when other people glance at the drawing, they will
                more or less know the meaning and model of the drawing, namely
                AutoCAD. Those of civil engineering alumni, engineers,
                architects, and anyone who likes to sketch houses and rooms know
                what AutoCAD is. Because the existence of this software makes
                the work they do both in making 2D or 3D images easier and
                lighter. There are first steps you can take to master AutoCAD,
                namely by getting to know the tools they have. Even though it
                looks trivial, the existing tools in AutoCAD are proven to make
                everything easier to do, including:
              </p>
              <p>
                <b>1. Line</b>
              </p>
              <p>
                Line, as the name implies, the Line command (which means “line”)
                in 2-dimensional design using AutoCAD functions to create
                straight-line objects, objects are formed by determining the
                starting point or node and the end node, this command can be
                accessed by typing “L” or “Line” then press the Enter key.
              </p>
              <p>
                <b>2. Arc</b>
              </p>
              <p>
                After that there is the Arc command on 2-dimensional object
                design using AutoCAD to create a semicircular object (“arc”
                which means “ark” or “boat” is considered to represent a
                semicircle), this command can be accessed by typing “Arc” then
                press the Enter key.
              </p>
              <p>
                <b>3. Fillet</b>
              </p>
              <p>
                The Fillet command in 2-dimensional object design using AutoCAD
                is used to make objects with rounded edges (curved), the degree
                of tilt of the angle is determined based on the radius or
                dimensions of the circle, this command can be accessed by typing
                “Fillet” then pressing the Enter key.
              </p>
              <p>
                <b>4. Chamfer</b>
              </p>
              <p>
                Unlike the Fillet command, the Chamfer command on 2-dimensional
                object design using AutoCAD is used to create straight-angled
                objects, the degree of tilt of the angle is determined based on
                the coordinates on the x-axis and y-axis, this command can be
                accessed by typing & “Chamfer” then pressing the Enter key. With
                the development of technology today, designing a house can not
                only be done in a traditional way with minimal visualization.
                However, with the advancement of technology today, there are
                software and devices that can simplify their work for interior
                designers. AutoCAD with all the tools it has can make the job
                easier. Time-saving, interior design wherever you want, even on
                the way, is not a strange thing anymore.
              </p>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default ArticleGeneral2;
