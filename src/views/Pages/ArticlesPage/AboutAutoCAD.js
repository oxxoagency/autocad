import React from "react";

import { Container, Card, Button, Media } from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

import Image1 from "../../../assets/img/content_about_autocad_1.png";
import Image2 from "../../../assets/img/content_about_autocad_2.png";
import Image3 from "../../../assets/img/content_about_autocad_3.png";
import Image4 from "../../../assets/img/content_about_autocad_4.png";
import Image5 from "../../../assets/img/content_about_autocad_5.png";
import Image6 from "../../../assets/img/content_about_autocad_6.png";
import Image7 from "../../../assets/img/content_about_autocad_7.png";
import Image8 from "../../../assets/img/content_about_autocad_8.png";
import Image9 from "../../../assets/img/content_about_autocad_9.png";

const ContainerCustom = styled(Container)`
  padding: 0px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 50px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const ImgWrapper = styled.div`
  padding: 20px;
  text-align: center;
`;

const ImgCustom = styled.img`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const AboutAutoCADArticles = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Media>
            <Media body>
              <Media heading>Apa fitur terbaru Autocad 2021?</Media>
              <p>
                Setiap tahun Autodesk merilis versi terbaru dari autoCAD, untuk
                tahun ini ada beberapa fitur baru yang dapat membantu pekerjaan
                anda lebih baik lagi.
              </p>
              <p>
                <b>Drawing History</b>, Fitur ini memiliki kemampuan untuk
                melihat riwayat gambar yang sudah dibuat ketika anda
                menyimpannya sehingga anda bisa mengetahui kapan gambar tersebut
                diperbarui. Tidak hanya itu riwayat perubahannya bisa kita
                ketahui dengan cara membandingkannya dengan versi sebelumnya.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image1}
                  alt="image_1"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Xref Compare</b>, fitur ini mampu membandingkan file
                referensi external (Xref) tanpa harus keluar dari file gambar
                yang sedang anda kerjakan. Sama dengan fitur “DWG Compare” bisa
                mengetahui perubahan gambar yang sedang dibandingkan.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image2}
                  alt="image_2"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Trim dan Extend yang dikembangkan</b>, fitur ini menjadi
                lebih pintar dari sebelumnya karena otomatis langsung memilih
                mana objek yang dipotong/diperpanjang sehingga mempermudah
                pekerjaan dalam membuat gambar.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image3}
                  alt="image_3"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Peningkatan perintah Quick Measure Tool</b>, fitur ini dapat
                digunakan untuk melihat ukuran-ukuran secara langsung pada
                gambar dengan hanya menggeser pointer. AutoCAD akan menampilkan
                pengukuran secara langsung ke geometri terdekat dari pointer
                anda dan ditambahkan kemampuan untuk melihat luas area pada
                sebuah objek tertutup dan mengakumulasi semua area yang dipilih
                secara cepat dan mudah.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image4}
                  alt="image_4"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Peningkatan perintah Block Palette</b>, memasukan “blocks”
                lebih efisien karena disediakan palette khusus untuk “block”
                dengan tambahan fitur untuk meng-copy, rotasi, skala, dan
                “explode block” pada dialog box khusus. Fitur ini dikembangan
                lagi karena bisa diakses dari library desktop, mobile app,
                maupun web dan bisa diakses dimana saja dan kapan saja sehingga
                memudahkan pengguna autoCAD.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image5}
                  alt="image_5"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Peningkatan fitur Revision Cloud</b>, fitur ini adalah
                pengembangan dari fitur sebelumnya yang ditambahkan pengaturan
                panjang busur yang bisa disesuaikan dan otomatis menyesuaikan
                dengan panjang total dari cord diagonalnya.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image6}
                  alt="image_6"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Cloud Storage Connectivity</b>, memungkinkan untuk mengakses
                file DWG apapun dengan provider yang berbeda, dengan adanya
                fitur tambahan ini mampu menambah kapasitas storage bawaan dari
                Autodesk (25 gigabytes) sehingga lebih banyak file yang bisa
                diakses.
              </p>
              <p>
                <b>Break Objects at a Single Point</b>, fitur ini digunakan
                untuk memisahkan garis lurus, garis lengkung, atau polyline
                menjadi 2 objek yang dipilih kemudian dapat diulang perintah
                tersebut dengan menekan “enter”.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image7}
                  alt="image_7"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Peningkatan fitur AutoLISP</b>, fitur autoLISP saat ini sudah
                bisa menggunakan Microsoft Visual Studio Code untuk edit dan
                debug kode autoLISP, sehingga mudah untuk edit kode autoLISP dan
                juga mudah mencari dan mengurangi bug yang ada pada kode
                AutoLISP.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image8}
                  alt="image_8"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Tampilan AutoCAD</b>, autodesk selalu memiliki tampilan yang
                “diperbarui”. Sekarang tampak lebih tajam dan lebih nyaman untuk
                dilihat. Versi ini diklaim bekerja lebih cepat dibandingkan
                versi sebelumnya. Bahkan instalasinya lebih cepat. Perbaikan ini
                cukup penting. Perbaikan performa yang dapat diandalkan, lebih
                cepat dan jarang hang.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image9}
                  alt="image_9"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default AboutAutoCADArticles;
