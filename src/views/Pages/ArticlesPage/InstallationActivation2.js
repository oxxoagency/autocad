import React from "react";

import { Container, Card, Media, Button } from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

const ContainerCustom = styled(Container)`
  padding: 50px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 40px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const ArticleInstallation2 = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Media>
            <Media body>
              <Media heading>
                <p>Perubahan kebijakan support Autodesk</p>
                <p style={{ fontSize: "15px" }}>
                  Mulai 31 Agustus 2019, Autodesk mulai member­ lakukan
                  kebijakan "technical support lifecycle" untuk semua versi
                  terbaru software Autodesk dan versi sebelumnya (biasanya tiga
                  versi dari yang terbaru), masuk pada daftar "Eligibility
                  subscrip ­ tion and maintenance plan".
                </p>
              </Media>
              Pelanggan dapat terus menggunakan versi bera­ papun dari license
              perpetual/permanen/lifetime yang maintenance-nya tidak aktif,
              selama itu <b>tidak melanggar ketentuan penggunaan produk</b>.
              Autodesk memilih untuk membuat perubahan dalam proses aktivasi
              produk mereka karena se­ bagian besar generator kode aktivasi
              autodesk harus pensiun (teknologi yang sudah mengalami penuaan),
              beberapa di antaranya tidak lagi didukung oleh vendor yang
              membangunnya. Seh­ ingga Autodesk harus menyelaraskan dengan te­
              knologi yang terbaru maka beberapa teknologi generator sebelumnya
              harus pensiun untuk men­gurangi risiko dan memastikan bahwa
              Autodesk memberikan bantuan service yang terbaik sesuai kebutuhan
              pelanggan Autodesk . Autodesk memberikan saran terbaik untuk
              mendapatkan versi software sesuai dengan kebu­tuhan pelanggan. Ada
              banyak opsi pembelian produk Autodesk beberapa promo terkait
              produk autodesk. Silahkan kunjungi link www.au­todesk.com untuk
              mengetahui lebih lanjut terkait produk Autodesk.
              <p>
                Apakah mungkin untuk meminta kode aktivasi & menyimpannya untuk
                digunakan jika dibutuhkan?
              </p>
              <p>
                Bisa dilakukan , tetapi hanya dalam parameter yang dikontrol
                secara ketat di mana konfigurasi tidak berubah dari saat kode
                dihasilkan. Autodesk tidak dapat menjamin bahwa kode yang dibuat
                sebelumnya akan berfungsi sebagaimana dimak­ sud. Autodesk tidak
                akan menawarkan aktivasi produk atau dukungan instalasi untuk
                produk yang tidak ada dalam daftar versi sebelumnya yang
                memenuhi syarat untuk support "subscrip­ tion dan maintenance
                plan".
              </p>
              <p>
                <b>
                  Mulai 31 Agustus 2019 semua software Autodesk
                  perpetual/permanen/l ifetime yang off mainte­ nance untuk
                  versi 2010 atau sebelumnya sudah tidak di support aktivasinya
                  (sudah tidak dapat diaktifkan) . Pelanggan masih bisa membuka
                  file gambarnya karena software Autodesk yang ter­ baru bisa
                  membaca file gambar versi sebelumnya. Semua software
                  perpetual/permanen/l ifetime yang off maintenance untuk versi
                  2011 dan setelahnya hanya akan disupport aktivasinya sampai
                  Maret 2021 dan sesuai dengan ketentuan penggunaan produk
                  Autodesk. Jika aktivasi produk tersebut melewati batas akhir
                  dari waktu yang ditentuan oleh Autodesk maka pelanggan hanya
                  dapat menggunakan versi yang terakhir di­ aktifkan dan harus
                  menghapus instalasi versi lain.
                </b>
              </p>
              <p>
                Misalnya, jika AutoCA D 2020 adalah versi saat ini ketika paket
                Anda dihentikan (off maintenance), tetapi Anda terakhir
                mengaktifkan AutoCA D 2018, Anda hanya dapat menggunakan AutoCA
                D 2018 . Jika Anda meminta kode aktivasi untuk AutoCA D 2018 ,
                versi catatan Anda menjadi AutoCA D 2018, dan kami akan memberi
                Anda kode aktivasi. Anda dapat terus mendapatkan dukungan teknis
                untuk AutoCAD 2018 sampai tidak lagi terdaftar pada daftar versi
                yang memenuhi syarat untuk mendap­ at support autodesk .
              </p>
              <p>
                Media kit/source/installer bisa meminta langsung ke Autodesk
                berupa link download ( softcopy ) sesuai dengan ketentuan versi
                yang disupport oleh autodesk (3 versi dari versi terbaru) tidak
                support untuk media fisik hardcopy berupa disk/­
                flashdisk/dongle. Permintaan media kit hardcopy berupa
                disk/flashdisk/dongle dapat dibeli sesuai dengan kebutuhan.
              </p>
              <p>
                <b>Note :Kebijakan ini berlaku untuk semua produk Autodesk.</b>
              </p>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default ArticleInstallation2;
