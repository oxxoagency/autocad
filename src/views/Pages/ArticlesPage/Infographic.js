import React from "react";

import { Container, Card, CardImg, Media } from "reactstrap";

import styled from "styled-components";

import Article1 from "../../../assets/img/article_1.jpg";

const ContainerCustom = styled(Container)`
  padding: 50px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 50px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
    overflow: auto;
    border: none;
  }
`;

const Infographic = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <CardImg top width="100%" src={Article1} alt="Card image cap" />
          <Media>
            <Media body>
              <Media heading>
                Apa benefit autoCAD Including Specialized Toolset 2021?
              </Media>
              <ul>
                <li>
                  AutoCAD 2021 dirilis pada maret 2020. Software yang sudah
                  lebih dari 30 tahun digunakan oleh profesional di bidang
                  arsitektur dan bidang manufacture ini memiliki pembaruan
                  besar. Setelah melakukan penyempurnaan selama beberapa dekade,
                  pada versi baru ini, pengguna bisa mendapatkan akses ke tujuh
                  set alat profesional (toolset) sekaligus yang dilengklapi
                  dengan 750.000 fitur dengan fungsi yang berbeda-beda, ditambah
                  dengan akses AutoCAD web dan mobile apps. Tujuh tool tersebut
                  di antaranya Architecture, Mechanical, Electrical, Map 3D,
                  MEP, Raster Design, dan Plant 3D. Sementara itu, disediakan
                  ribuan fitur lainnya berfungsi untuk membantu pengguna dalam
                  menyelesaikan tugas-tugas khusus, seperti menggambar layout
                  panel listrik dengan cepat, bekerja dengan data Geographic
                  Information System (GIS), serta mengedit gambar yang dipindai.
                </li>
              </ul>
            </Media>
          </Media>
        </CardCustom>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default Infographic;
