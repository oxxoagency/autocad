import React from "react";
import { Container, Card, Media, Button } from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

import Image1 from "../../../assets/img/content_general_4_1.png";

const ContainerCustom = styled(Container)`
  padding: 0px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 40px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const ImgWrapper = styled.div`
  padding: 0px;
  text-align: center;
`;

const ImgCustom = styled.img`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const ArticleGeneral4 = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Media>
            <Media body>
              <ImgWrapper>
                <ImgCustom
                  src={Image1}
                  alt="image_1"
                  style={{ width: "100%" }}
                ></ImgCustom>
              </ImgWrapper>
              <Media heading>
                Get to Know Revit : Special App on Architecture
              </Media>
              <p>
                Having an own building is a dream for some people because many
                think that having a building in their own name is a form of
                independence and stability. However, often when building a house
                or building there are various obstacles that cause construction
                to be delayed. The most common thing is financial problems.
                Nowadays money is an important thing to support life, almost all
                aspects of life require money, including for development. So,
                the budget is an important thing to consider when building a
                house, expenses, and income must be adjusted to the desired
                building design.
              </p>
              <p>
                Apart from the budget, there are other things that are no less
                important in the development process, namely design. There are
                so many people who always cancel building a house due to a
                design that is not in accordance with the wishes and budget they
                have. There are also some companies that do not accept
                construction engineer sketches due to unclear drawings. These
                various problems are of course burdensome to building architects
                who have to be confused in determining a house design that is
                according to budget, attractive, and with a clear depiction.
              </p>
              <p>
                One of the leading design software companies, Autodesk, is one
                of the designer’s choices for making sketches of their designs,
                namely AutoCAD. Apart from AutoCAD, which is Autodesk’s
                primadonna, there are other applications developed by the
                company specifically for doing architectural work. This
                application is called Revit which was introduced by Autodesk in
                2004. Like other Autodesk products, Revit is a software used to
                create 2D and 3D-based designs, the difference with the AutoCAD
                version is that Revit supports the BIM or Building Information
                Modeling program, a technology that can simulate all project
                information in a 3-dimensional model.
              </p>
              <p>
                Revit is equipped with a general feature that can change the
                sketch into a lightened image and is in accordance with the
                designer&#39;s goals, apart from generating Revit it is also
                supported by the parametric components feature so that designers
                don&#39;t have to bother using coding to change the position of
                related image objects, for example when they want to move a
                door. From one side to the other, the walls associated with the
                door will automatically move according to the position. This
                makes the design job easier and more effective. Sometimes the
                actual amount of expenditure with planning has a much different
                nominal, but with the estimated schedule feature of the number
                of workers, the amount of material and expenses can be precisely
                adjusted using Revit. In addition to the schedule feature, Revit
                is also equipped with a global parameter feature that makes
                approximate building arrangements according to what is in the
                field.
              </p>
              <p>
                Worksharing makes teamwork less of an obstacle, this is because
                this feature allows each computer unit in a network to access
                the same files from the central computer data so that every time
                there is a revision, every computer unit will be updated
                automatically. In addition to files that will be updated
                automatically, Revit also allows file exchange from various
                computers to be easier. This is because Revit has
                Interoperability and IFC so that files can be imported and
                exported in various file formats according to IFC’s SMART
                building standards. The use of Revit can also maintain
                communication between designers and clients because it is able
                to display good and attractive images and is easy to understand.
                Also, the annotation feature makes it easier for the designer to
                note the revisions given by the client.
              </p>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default ArticleGeneral4;
