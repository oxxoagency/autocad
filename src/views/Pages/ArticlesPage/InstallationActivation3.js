import React from "react";

import { Container, Card, Button, Media, Row, Col } from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

import Image1 from "../../../assets/img/content_article_installation_3_1.png";
import Image2 from "../../../assets/img/content_article_installation_3_2.png";
import Image3 from "../../../assets/img/content_article_installation_3_3.png";
import Image4 from "../../../assets/img/content_article_installation_3_4.png";
import Image5 from "../../../assets/img/content_article_installation_3_5.png";
import Image6 from "../../../assets/img/content_article_installation_3_6.png";
import Image7 from "../../../assets/img/content_article_installation_3_7.png";
import Image8 from "../../../assets/img/content_article_installation_3_8.png";
import Image9 from "../../../assets/img/content_article_installation_3_9.png";
import Image10 from "../../../assets/img/content_article_installation_3_10.png";
import Image11 from "../../../assets/img/content_article_installation_3_11.png";
import Image12 from "../../../assets/img/content_article_installation_3_12.png";
import Image13 from "../../../assets/img/content_article_installation_3_13.png";
import Image14 from "../../../assets/img/content_article_installation_3_14.png";
import Image15 from "../../../assets/img/content_article_installation_3_15.png";

const ContainerCustom = styled(Container)`
  padding: 0px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 50px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const ImgWrapper = styled.div`
  padding: 20px;
  text-align: center;
`;

const ImgCustom = styled.img`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const ArticleInstallation3 = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Media>
            <Media body>
              <Media heading>
                Panduan singkat download, instalasi, dan aktivasi AutoCAD Single
                User
              </Media>
              <p>
                Mulai 1 Febuari 2016 Autodesk metransformasi tipe license mereka
                dari perpetual menjadi Desktop Subscription. Transformasi ini
                membuat perubahan pada cara aktivasinya. License autodesk yang
                dulu namanya “stand alone” sekarang menjadi “single user” dan
                yang dulu namanya “Network” sekarang menjadi “multi user”.
                License autodesk saat ini mengalami perubahan sistem sehingga
                banyak pengguna mengalami kebingungan untuk download, install,
                dan aktivasi software autodesk. Panduan singkat ini semoga bisa
                membantu para pengguna untuk bisa download, instal, dan aktivasi
                software autodesk. Tulisan ini tidak membahas tentang benefit
                autodesk subscription tetapi lebih memfokuskan pada bagaimana
                pengguna bisa download, instal, dan aktivasi license.
              </p>
              <p>
                <b>Account Autodesk</b>
              </p>
              <p>
                Account autodesk adalah email ID yang didaftarkan ke autodesk
                untuk bisa masuk ke portal autodesk dan semua yang berhubungan
                dengan cloud services autodesk. Account autodesk ada beberapa
                tipe diantaranya :
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image1}
                  alt="image_1"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                Pada pembelian software autodesk Email ID yang didaftarkan
                sebagai “contract manager”, jika belum pernah didaftarkan ke
                autodesk maka akan mendapatkan email verifikasi untuk mendaftar
                ke portal autodesk Jika sudah pernah terdaftar maka hanya akan
                mendapat pemberitahuan bahwa email tersebut didaftarkan sebagai
                “contract manager”.
              </p>
              <p>
                <b>Download Software</b>
              </p>
              <p>
                Secara default Email ID yang baru pertama kali didaftarkan akan
                ada email dari Autodesk berupa link verifikasi untuk mendaftar
                ke portal autodesk. Jika tidak ada cek pada spam, jika tidak
                menerima link verifikasi dari Autodesk maka langsung sign saja
                ke portal Autodesk (
                <span style={{ color: "red" }}>
                  https://accounts.autodesk.com
                </span>
                ) dan pilih menu “forgot”.
              </p>
              <p>
                <ul>
                  <li>
                    Setelah pilih menu tersebut akan ada email dari Autodesk
                    untuk reset password
                  </li>
                  <li>
                    Jika ada kendala “sign in” ke autodesk segera hubungi
                    partner/reseller autodesk
                  </li>
                  <li>
                    Jika sudah berhasil masuk ke account Autodesk pilih menu “
                    <span style={{ color: "red" }}>
                      Management product and download
                    </span>
                    ”
                  </li>
                </ul>
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image2}
                  alt="image_2"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <ul>
                  <li>
                    Kemudian pilih menu “
                    <span style={{ color: "red" }}>Product & Services</span>”{" "}
                    {"->"}
                    pilih dan click product yang di inginkan kemudian Expand
                    product tersebut dan click download
                  </li>
                </ul>
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image3}
                  alt="image_3"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                Pilih versi, platform, dan language yang diinginkan. Ada tiga
                pilihan jika diexpand atau diclick “view all” pada bagian kanan.
              </p>
              <p>
                <ul>
                  <li>
                    Install Now {"->"} menginstal langsung software dan
                    mendownloadnya secara bersamaan, cara ini tidak recommended
                    jika connection jaringan internet lambat.
                  </li>
                  <li>
                    Download Now {"->"} Mendownload dan menyimpan source
                    original Software yang sudah ter-extract di local disk, cara
                    ini juga kurang recommended jika jaringan connection
                    internet sering mengalami problem (putus-putus) karna jika
                    connection internet putus saat download belum complete akan
                    mengakibatkan source corrupt.{" "}
                  </li>
                  <li>
                    Browser Download {"->"} Mendownload dan menyimpan source
                    software dalam bentuk file .exe, file mentahan yang belum
                    ter-extract {"->"} cara ini yang di recommended untuk
                    mendownload source product karena selain capasitas download
                    yang lebih kecil dari cara-cara sebelumnya dengan cara ini
                    dapat meminimalisir resiko terjadinya corrupt pada source
                    software saat mendownload. Pastikan pop up browser
                    dinonaktifkan.
                  </li>
                </ul>
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image4}
                  alt="image_4"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                Setelah melakukan download source software yang sudah tersimpan
                di local disk (file mentah yang belum ter-extract / .exe file
                langkah selanjutnya adalah meng-extract file tersebut di local
                disk atau bisa juga di external disk / USB), Pilih dan double
                click salah satu file .exe {"->"} maka file tersebut akan
                mengextract dan mengarahkan file location yang di extract secara
                otomatis.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image5}
                  alt="image_5"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Instalasi Software</b>
              </p>
              <p>
                Setelah proses Extract complete langkah selanjutnya kita dapat
                menginstal software di Computer User, dan berikut adalah
                langkah-langkah instalasi Autodesk AutoCAD. Sebelum menginstal
                software autodesk pastikan “system requirement” dari perangkat
                yang akan diinstal sudah sesuai dengan requirement resmi
                autodesk. Silahkan search menggunakan browser dengan kata kunci
                “requirement autoCAD 2021” atau produk autodesk yang lain. Jika
                sudah sesuai dengan requirement autodesk maka bisa dilakukan
                instalasi.
              </p>
              <p>
                Open source file location hasil extract file, pilih dan double
                click setup.exe, pastikan source file yang akandi install sesuai
                dengan OS (Operation System) source 32 bit untuk Operating
                System 32 bit dan source 64 bit untuk Operating System 64 bit
                dan jika menggunakan Operating System windows 8 atau Windows 10
                gunakan Source 64 bit dan jika user menggunakan MAC gunakan
                source Autodesk product AutoCAD for MAC
              </p>
              <Row style={{ marginTop: "20px", marginBottom: "20px" }}>
                <Col>
                  <img
                    src={Image6}
                    alt="image_6"
                    style={{ width: "30vw" }}
                  ></img>
                </Col>
                <Col>
                  <img
                    src={Image7}
                    alt="image_7"
                    style={{ width: "30vw" }}
                  ></img>
                </Col>
              </Row>
              <p>
                <ul>
                  <li>
                    Pilih menu install untuk proses instalasi kemudian tentukan
                    Country of Region sesuai dengan Location {"->"} pilih atau
                    click I Accept kemudian Next
                  </li>
                  <li>
                    Click Setting dan configure software, pastikan semua feature
                    ter-checklist
                  </li>
                  <li>Langkah selanjutnya adalah pilih dan click install </li>
                </ul>
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image8}
                  alt="image_8"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <ul>
                  <li>
                    Kemudian tunggu sampai proses selesai dan kemudian pilih
                    finish
                  </li>
                </ul>
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image9}
                  alt="image_9"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Aktifkan License</b>
              </p>
              <p>
                Aktivasi license adalah proses validasi antara produk dengan
                license yang dimiliki pengguna. Aktivasi Autodesk juga mengalami
                perubahan “tidak lagi memasukan serial number” tetapi langsung
                sign in menggunakan email ID yang sudah diberi akses license
                oleh admin. Setelah selesai install Open software AutoCAD
                kemudian pilih menu “Single User” dan atau “Sign In”.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image10}
                  alt="image_10"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                Kemudian masukan Email ID & password sesuai dengan account yang
                diberi akses oleh admin software
              </p>
              <Row style={{ marginTop: "20px", marginBottom: "20px" }}>
                <Col>
                  <img
                    src={Image11}
                    alt="image_11"
                    style={{ width: "25vw", height: "25vh" }}
                  ></img>
                </Col>
                <Col>
                  <img
                    src={Image12}
                    alt="image_12"
                    style={{ width: "25vw", height: "25vh" }}
                  ></img>
                </Col>
              </Row>
              <p>
                Secara Automatis software akan terbuka dan software Autodesk
                (AutoCAD) sudah dapat digunakan.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image13}
                  alt="image_13"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Akses License</b>
              </p>
              <p>
                Email ID yang digunakan untuk aktivasi harus sudah mendapatkan
                akses license dari admin berikut cara untuk memberikan akses
                license kepada user dari account admin. Pada menu “Manage
                Product and Download” pilih menu “User Management” kemudian
                pilih “Add User”
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image14}
                  alt="image_14"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <ul>
                  <li>
                    Masukan dengan format “first name, Last name, Email ID”
                    kemudian pilih “send invite”. Autodesk akan mengirim email
                    ke user untuk masuk ke portal autodesk.
                  </li>
                  <li>
                    Memberikan akses license/produk ke user dengan memilih menu
                    By Product kemudian pilih produk yang akan di assign ke
                    user.
                  </li>
                </ul>
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image15}
                  alt="image_15"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default ArticleInstallation3;
