import React from "react";
import { Container, Card, Media, Button } from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

import Image1 from "../../../assets/img/content_general_5_1.png";

const ContainerCustom = styled(Container)`
  padding: 0px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 40px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const ImgWrapper = styled.div`
  padding: 0px;
  text-align: center;
`;

const ImgCustom = styled.img`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const ArticleGeneral5 = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Media>
            <Media body>
              <ImgWrapper>
                <ImgCustom
                  src={Image1}
                  alt="image_1"
                  style={{ width: "100%" }}
                ></ImgCustom>
              </ImgWrapper>
              <Media heading>
                AutoCAD More Value Compared with Other Software
              </Media>
              <p>
                The increasingly competitive labor force in the engineering
                world makes many people compete to improve their abilities. It
                is inevitable that many industry players have started to pursue
                new things. One of them is in terms of design. Today’s
                sophisticated technology makes people not have to bother with
                drawing manually. With a laptop and mouse, we can make
                interesting designs using the AutoCAD application.
              </p>
              <p>
                AutoCAD as the software is designed to design or compile images
                in two forms, namely 2D and 3D. This software is generally used
                to design houses, buildings, or other buildings. Architects
                often use AutoCAD software to sketch houses or buildings to be
                made. Apart from architects, general AutoCAD users are people
                who are involved in the world of civil engineering, electricity,
                and machinery. In addition, AutoCAD can also be used as a tool
                for engineering mechanics calculations. For example, calculating
                the rod force on the roof truss structure by means of the
                graphic or the Cremona method. This is because the angle can be
                calculated accurately and precisely.
              </p>
              <p>
                AutoCAD allows its users to make various designs such as
                airplanes, cars, and engine designs. Because of its various
                uses, the AutoCAD software has been very popular since its
                release in 1982. The AutoCAD development company also continues
                to make updates to this software working system. One of the
                reasons why AutoCAD has become popular and favored by many
                parties is its very helpful function for work. Among others are:
              </p>
              <p>
                <b>1. Shorten Time</b>
              </p>
              <p>
                With AutoCAD, the process of creating a design or drawing is
                much shorter. Software that is simple and easy to understand
                makes AutoCAD simplify design work. Previously, drawing designs
                manually took days, now with AutoCAD, we can do this in a matter
                of hours with the help of software.
              </p>
              <p>
                <b>2. Easy to be Saved</b>
              </p>
              <p>
                If we want to print the results of the designs that have been
                made, AutoCAD makes it easy to do these commands. Steps that can
                be taken, namely export the design, then save it to a computer
                or flash. Apart from that, this software also offers efficiency
                in terms of plotting. The designs that have been made can be
                easily documented and printed.
              </p>
              <p>
                <b>3. Simplify Editing</b>
              </p>
              <p>
                One of the advantages of AutoCAD is the ease of editing or
                editing. There are several features such as erase, copy, break,
                extend, and so on to make the work that we do easier. Editing
                features are very important for a design. The high level of
                complexity does not rule out the possibility of making us often
                make mistakes in design. Moreover, work in terms of design does
                require perfection, both in size and in visual images. So,
                AutoCAD has several easy-to-use tools for the editing process.
              </p>
              <p>
                <b>4. More Accurate Design</b>
              </p>
              <p>
                AutoCAD applications can create designs that are more precise
                and accurate than drawing manually. This software can scale an
                image according to its original size with precision, even if you
                want to use it to make complex buildings. The manual method will
                most likely find it difficult to make these design drawings on a
                large scale. Because related to the details, the size must be
                calculated by self-calculation. Meanwhile, AutoCAD has prepared
                features and tools to make it easier for users of the software.
              </p>
              <p>
                <b>Conclusion</b>
              </p>
              <p>
                In a world that is increasingly sophisticated in terms of
                technology like today, a design job does not have to be done
                manually as in the previous era. The increasingly sophisticated
                technology has led to various innovations to facilitate design
                work. One of them is the AutoCAD software. This software is
                designed according to the needs of interior designers for both
                buildings and others and is equipped with various tools to make
                work easier. Moreover, the job is demanded to be perfect and
                minimal risk.
              </p>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default ArticleGeneral5;
