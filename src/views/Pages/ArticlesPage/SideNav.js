import React, { useState } from "react";

import {
  Nav,
  NavItem,
  NavLink,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

import styled from "styled-components";

const NavCustom = styled(Nav)`
  @media screen and (min-width: 1801px) {
    padding: 50px;
    position: fixed;
  }

  @media screen and (max-width: 1800px) {
    padding-bottom: 10px;
  }
`;

const NavLinkCustom = styled(NavLink)`
  color: black;
  :hover {
    color: red;
  }
`;

const DropdownToggleCustom = styled(DropdownToggle)`
  color: black;
  :hover {
    color: red;
  }
`;

const LineBreakSideBar = styled.hr`
  width: 300px;
  float: left;
  margin-left: 20px;
`;

const SideNavProductsPage = props => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [dropdownOpen1, setDropdownOpen1] = useState(false);

  const toggle = () => setDropdownOpen(!dropdownOpen);
  const toggle1 = () => setDropdownOpen1(!dropdownOpen1);

  return (
    <React.Fragment>
      <style>
        {`.active  > .nav-link {
              color: red;
            }`}
      </style>
      <NavCustom vertical>
        <NavItem
          active={
            props.location === "/articles/general" ||
            props.location === "/articles/general/1/1" ||
            props.location === "/articles/general/1/2" ||
            props.location === "/articles/general/1/3" ||
            props.location === "/articles/general/1/4" ||
            props.location === "/articles/general/1/5"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/articles/general">General</NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <Dropdown nav isOpen={dropdownOpen} toggle={toggle}>
          <DropdownToggleCustom nav caret>
            Instalasi dan Aktivasi
          </DropdownToggleCustom>
          <DropdownMenu>
            <DropdownItem
              href="/articles/installation_and_activation/1"
              active={
                props.location === "/articles/installation_and_activation/1"
                  ? true
                  : false
              }
            >
              Berakhirnya Support Autodesk untuk versi 2010
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem
              href="/articles/installation_and_activation/2"
              active={
                props.location === "/articles/installation_and_activation/2"
                  ? true
                  : false
              }
            >
              Perubahan Kebijakan Support Autodesk
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem
              href="/articles/installation_and_activation/3"
              active={
                props.location === "/articles/installation_and_activation/3"
                  ? true
                  : false
              }
            >
              Tutorial Download Instalasi dan Aktivasi Autodesk
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
        <LineBreakSideBar />
        <NavItem
          active={
            props.location === "/articles/transition_to_name_user"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/articles/transition_to_name_user">
            Transisi lisensi Autodesk ke Named User
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <Dropdown nav isOpen={dropdownOpen1} toggle={toggle1}>
          <DropdownToggleCustom nav caret>
            AutoCAD
          </DropdownToggleCustom>
          <DropdownMenu>
            <DropdownItem
              href="/articles/new_features_2021"
              active={
                props.location === "/articles/new_features_2021" ? true : false
              }
            >
              Apa Fitur Terbaru AutoCAD 2021
            </DropdownItem>
            {/* <DropdownItem divider /> */}
            {/* <DropdownItem
              href="/articles/about_autocad_including_specialized_toolset_2021"
              active={
                props.location ===
                "/articles/about_autocad_including_specialized_toolset_2021"
                  ? true
                  : false
              }
            >
              AutoCAD Including Specialized Toolset 2021
            </DropdownItem> */}
          </DropdownMenu>
        </Dropdown>
        {/* <NavItem
          active={
            props.location === "/articles/autocad_infographic" ? true : false
          }
        >
          <NavLinkCustom href="/articles/autocad_infographic">
            AutoCAD Infographic
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem> */}
        {/* <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/architecture_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/architecture_toolset">
            Architecture Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/electrical_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/electrical_toolset">
            Electrical Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/mechanical_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/mechanical_toolset">
            Mechanical Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/map3d_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/map3d_toolset">
            Map 3D Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/mep_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/mep_toolset">
            MEP Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/plant3d_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/plant3d_toolset">
            Plant 3D Toolset
          </NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem
          active={
            props.location ===
            "/products/autocad_including_specialized_toolset_2021/rasterdesign_toolset"
              ? true
              : false
          }
        >
          <NavLinkCustom href="/products/autocad_including_specialized_toolset_2021/rasterdesign_toolset">
            Raster Design Toolset
          </NavLinkCustom>
        </NavItem> */}
      </NavCustom>
    </React.Fragment>
  );
};

export default SideNavProductsPage;
