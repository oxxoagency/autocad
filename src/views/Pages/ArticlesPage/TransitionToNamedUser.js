import React from "react";
import { Container, Card, Media, Button } from "reactstrap";

import styled from "styled-components";

import BackToTop from "react-back-to-top-button";

import Image1 from "../../../assets/img/content_article_transition_1.png";
import Image2 from "../../../assets/img/content_article_transition_2.png";
import Image3 from "../../../assets/img/content_article_transition_3.png";

const ContainerCustom = styled(Container)`
  padding: 0px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 40px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const ImgWrapper = styled.div`
  padding: 20px;
  text-align: center;
`;

const ImgCustom = styled.img`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const TransitionToNamedUser = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Media>
            <Media body>
              <img
                src={Image1}
                alt="image_1"
                style={{ width: "100%", marginBottom: "20px" }}
              ></img>
              <Media heading>
                Transisi Lisensi Autodesk ke program “Named User”
              </Media>
              <p>
                Pada 7 Mei 2020, Autodesk akan meluncurkan “
                <b>new subscription plans</b>” yang didasarkan pada setiap named
                user (nama pengguna) dan akan menghentikan license berdasarkan
                “Serial Number”. Seperti kebanyakan provider lain, perubahan ini
                dirancang untuk memberikan berbagai kemudahan pada user dalam
                mengelola licensi, mendapatkan support, dan mengeluarkan report
                penggunaan lisensi.
              </p>
              <p>
                Pelanggan sering mengalami kendala dalam mengelola licensi dan
                dalam mendistribusikan license ke user. Berikut ini adalah
                beberapa manfaat yang bisa didapatkan oleh pelanggan dengan
                adanya perubahan ke “Named user” :
                <ul style={{ marginTop: "30px" }}>
                  <li>
                    Mengoptimalkan biaya lisensi, dengan perubahan ini admin
                    user mampu melihat banyaknya licensi yang digunakan oleh
                    user dalam waktu tertentu sehingga perusahaan mampu
                    merencanakan budget pada “renewal” berikutnya dan mampu
                    mengoptimalkan biaya license yang akan dikeluarkan.
                  </li>
                  <li>
                    Tidak perlu mengelola licensi pada server lokal, atau
                    melacak “serial number” yang tidak terdeteksi.
                  </li>
                  <li>
                    Akses licensi untuk semua user yang membutuhkannya. Tidak
                    ada lagi downtime ketika server rusak atau harus diganti
                    karena versi yang sudah lama.
                  </li>
                  <li>
                    Support yang sudah dikembangkan dan juga ada tambahan fitur
                    untuk learning semua software Autodesk pada account user.
                  </li>
                </ul>
              </p>
              <p>
                Berikut rencana perubahan licensi ke “named user”:
                <ul style={{ marginTop: "30px" }}>
                  <li>
                    <b>
                      Licensi Subscription Multi User dan maintenance Network
                    </b>
                    akan dihentikan pada 7 Mei 2021 dan tidak dapat diperpanjang
                  </li>
                  <li>
                    Sebagai bagian dari license yang akan dihentikan, versi
                    terakhir dari “Design & Creation Suites” akan dirilis pada
                    bulan April 2020 dan bisa didownload oleh user melalui
                    account yang didaftarkan, dan tanggal terakhir untuk renewal
                    licensi tersebut adalah 16 April 2020.
                  </li>
                  <li>
                    Mulai 29 Februari 2020, Autodesk tidak akan lagi menjual “
                    <b>Multi User</b>” untuk 2 dan 3 tahunan baik pembelian baru
                    atau yang renewal. Mulai 7 Mei 2020, Autodesk tidak akan
                    lagi menjual licensi “<b>Multi User</b>” untuk pembelian
                    baru hanya melayani yang renewal satu tahunan.
                  </li>
                </ul>
              </p>
              <p>
                Untuk pelanggan yang memiliki licensi “
                <b>Multi User atau maintenance Network</b>”, pada renewal
                pertama setelah 7 Mei 2020, Autodesk membuat program “
                <b>Trade-In</b>” dengan biaya yang tidak jauh berbeda pada saat
                anda maintenance dan harga tersebut tetap konsisten sampai
                dengan 2028 *.
              </p>
              <p>
                <b>1. Penawaran “Standalone Maintenance” trade - in</b>
              </p>
              <p>
                Pada “<b>renewal</b>” pertama, sebelum 7 Mei 2021:
                <ul style={{ marginTop: "30px" }}>
                  <li>
                    Dengan melakukan “<b>Trade-In</b>” satu licensi “
                    <b>standalone maintenance</b>” dengan satu licensi standar
                    subscription “<b>named user</b>” harganya sama pada saat
                    anda renewal tahun 2019.
                  </li>
                  <li>
                    Kemudian bisa perpanjang dengan diskon khusus sampai dengan
                    tahun 2028.
                  </li>
                  <li>
                    Pada saat “<b>Trade-In</b>”, pilih untuk meningkatkan ke
                    produk AutoCAD Including Specialized Toolset untuk
                    mendapatkan pilihan benefit yang lebih banyak dan dapat
                    membantu Anda membuat disain yang memenuhi setiap tantangan
                    proyek baik sekarang maupun di masa depan.
                  </li>
                  <li>
                    Pada saat “<b>Trade-In</b>”, pilih untuk upgrade ke versi
                    yang lebih baru karena ada beberapa fitur baru yang lebih
                    baik dari versi sebelumnya, seperti sistem “Single Sign On”
                    dan report pengguna licensi yang lebih rinci dan detail.
                  </li>
                </ul>
              </p>
              <p>
                Bagaimana kalau tidak melakukan “<b>Trade-In</b>”?
              </p>{" "}
              <p>
                Jika Anda tidak melakukan “<b>Trade-In</b>”, harga mintenance
                akan naik 20% pada tanggal 7 Mei 2020. Licensi “
                <b>Multi User</b>”, “<b>maintenance Network</b>”, dan “
                <b>maintenance Standalone</b>” sudah tidak bisa diperpanjang
                lagi pada tanggal 7 Mei 2021.
              </p>
              <p>
                <b>
                  2. Penawaran “Multi-user atau Network Maintenance” trade-in
                </b>
              </p>
              <p>
                Pada “<b>renewal</b>” pertama, sebelum 7 Mei 2021:
                <ul style={{ marginTop: "30px" }}>
                  <li>
                    <b>
                      Trade-in” satu licensi subscription Multi User atau
                      maintenance network dengan dua licensi standar
                      subscription named user
                    </b>{" "}
                    dengan harga yang sama pada saat anda “<b>renewal</b>”.
                  </li>
                  <li>
                    Misalnya, jika Anda memiliki 20 licensi Multi User, Anda
                    dapat “<b>Trade-In</b>” untuk 40 licensi standar
                    subscription named user dengan harga yang sama dengan SRP
                    yang Anda bayar pada saat renewal.
                  </li>
                  <li>
                    Kemudian, “renewal/perpanjang” dengan diskon khusus hingga
                    2028.
                  </li>
                  <li>
                    Pada saat “<b>Trade-In</b>”, pilih untuk meningkatkan ke
                    produk AutoCAD Including Specialized Toolset untuk
                    mendapatkan pilihan benefit yang lebih banyak dan dapat
                    membantu Anda membuat disain yang memenuhi setiap tantangan
                    proyek baik sekarang maupun di masa depan.
                  </li>
                  <li>
                    Pada saat “<b>Trade-In</b>”, pilih untuk upgrade ke versi
                    yang lebih baru karena ada beberapa fitur baru yang lebih
                    baik dari versi sebelumnya, seperti sistem “Single Sign On”
                    dan report pengguna licensi yang lebih rinci dan detail.
                  </li>
                </ul>
              </p>
              <p>
                Bagaimana jika saya tidak melakukan{" "}
                <b>Trade-In licensi Multi User/Network</b> pada 7 mei 2020 ?
              </p>
              <p>
                <ul>
                  <li>
                    Jika Anda tidak melakukan “<b>Trade-in</b>” pertama (satu
                    license Multi User untuk 2 licensi subscription standar
                    named user), maka Anda akan diarahkan ke program “
                    <b>Trade-in</b>” selanjutnya :
                    <br />
                    <b>a. Satu licensi Multi User Subscription</b> untuk
                    mendapatkan diskon khusus dari Autodesk sebagai program
                    lanjutan dari “move to subscription”
                    <br />
                    <b>b. Satu licensi maintenance network</b> untuk{" "}
                    <b>satu subscription standar named user</b> sesuai dengan
                    harga SRP yang Anda bayar untuk existing licensi yang anda
                    miliki.
                  </li>
                  <li>
                    Jika Anda tidak melakukan “<b>Trade-In</b>” dan
                    mempertahankan licensi maintenance Network harga mintenance
                    akan naik 20% pada tanggal 7 Mei 2020. Licensi “
                    <b>Multi User</b>”, “<b>maintenance Network</b>”, dan “
                    <b>maintenance Standalone</b>” sudah tidak bisa diperpanjang
                    lagi pada tanggal 7 Mei 2021.
                  </li>
                </ul>
              </p>
              <p>
                <b>3. Berikut gambaran rencana transisi licensi Autodesk</b>
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image2}
                  alt="image_2"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>4. Produk Autodesk yang termasuk dalam program Trade-In</b>
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image3}
                  alt="image_3"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default TransitionToNamedUser;
