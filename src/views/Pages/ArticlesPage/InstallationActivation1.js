import React from "react";

import { Container, Card, Media } from "reactstrap";

import styled from "styled-components";

import Article1 from "../../../assets/img/Article_Installation_1.png";

const ContainerCustom = styled(Container)`
  padding: 0px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 40px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const ImgWrapper = styled.div`
  padding: 0px;
`;

const ImgCustom = styled.img`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const ArticleInstallation1 = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Media>
            <Media body>
              <ImgWrapper>
                <ImgCustom
                  src={Article1}
                  alt="article_1"
                  style={{ width: "100%" }}
                ></ImgCustom>
              </ImgWrapper>
              <Media heading>
                Software AutoCAD sudah tidak di support oleh Autodesk
              </Media>
              <br />
              Mulai 31 Agustus semua software autodesk versi 2010 dan sebelumnya
              sudah tidak di support oleh Autodesk ya. See you next soon.
            </Media>
          </Media>
        </CardCustom>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default ArticleInstallation1;
