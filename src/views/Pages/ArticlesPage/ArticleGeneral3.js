import React from "react";
import { Container, Card, Media, Button } from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

import Image1 from "../../../assets/img/content_general_3_1.png";

const ContainerCustom = styled(Container)`
  padding: 0px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 40px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const ImgWrapper = styled.div`
  padding: 0px;
  text-align: center;
`;

const ImgCustom = styled.img`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const ArticleGeneral3 = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Media>
            <Media body>
              <ImgWrapper>
                <ImgCustom
                  src={Image1}
                  alt="image_1"
                  style={{ width: "100%" }}
                ></ImgCustom>
              </ImgWrapper>
              <Media heading>Easy Designing on The Go</Media>
              <p>
                Human civilization is growing, making each era even more
                infinite. Some things that 10 years ago are still commonplace
                seem primitive in today’s era. For example, the use of paper. 20
                years ago the use of paper was as widespread as writing and
                drawing. Nowadays, with the development of increasingly advanced
                technology, the use of paper can be replaced by computers.
                Almost all jobs that used to prioritize paper as a medium have
                begun to switch to computers. No exception for mechanical
                engineers, architects, and constructors who have also long-
                abandoned paper as a drawing medium. They prefer to use computer
                software which is more practical than using paper. The need for
                an application that can help various kinds of design work is
                offset by the number of companies developing the application.
                One company that develops various applications in the design
                field is Autodesk with its flagship product AutoCAD. AutoCAD
                itself is a CAD computer software that can be used to draw
                designs in both 2D and 3D forms. AutoCAD has a lot of commands
                or commands and features that are very helpful for doing the
                drawing quickly and efficiently.
              </p>
              <p>
                The features and commands possessed by AutoCAD are also
                classified into several special toolsets such as architecture
                toolset, electrical toolset, map 3d toolset, MEP toolset,
                mechanical toolset, raster design toolset, and plant 3D toolset.
                So for new users, they don&#39;t have to bother looking for
                tools that suit their field of work. Because it has a variety of
                toolset that suits the realm of the designers&#39; work, of
                course, you can be sure that this application has very accurate
                precision accuracy. Images generated from this software have a
                precision of up to 13 levels. Even workers in the design field
                don&#39;t have to worry about not having enough empty space to
                draw the sketch. Autocad has a wide scope of work and seems
                unlimited. Projects of the scale of buildings, buildings, parks,
                even a country can be created using this application.
              </p>
              <p>
                As the name suggests, Autocad is commonly used in CAD computer
                devices with the windows operating system. And because AutoCAD
                is a software so it can only be used on one computer. However,
                in line with its current development, Autocad can be accessed
                via the web via the AutoCAD web app, so users don&#39;t have to
                install Autocad over and over again. Apart from the website,
                Autocad can also be accessed via a smartphone application,
                namely the Autocad mobile app. This application is available for
                Android and IoS users, so making sketching activities is not
                complicated and can be used anywhere.
              </p>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default ArticleGeneral3;
