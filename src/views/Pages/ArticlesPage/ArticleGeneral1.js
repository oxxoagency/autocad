import React from "react";
import { Container, Card, Media, Button } from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

import Image1 from "../../../assets/img/content_general_1_1.png";
import Image2 from "../../../assets/img/content_general_1_2.png";
import Image3 from "../../../assets/img/content_general_1_3.png";
import Image4 from "../../../assets/img/content_general_1_4.png";
import Image5 from "../../../assets/img/content_general_1_5.png";
import Image6 from "../../../assets/img/content_general_1_6.png";

const ContainerCustom = styled(Container)`
  padding: 0px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 40px;
    overflow: auto;
    border: none;
    line-height: 2.5;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const ImgWrapper = styled.div`
  padding: 20px;
  text-align: center;
`;

const ImgCustom = styled.img`
  margin-top: 20px;
  margin-bottom: 20px;
`;

const ArticleGeneral1 = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Media>
            <Media body>
              <ImgWrapper>
                <ImgCustom
                  src={Image1}
                  alt="image_1"
                  style={{ width: "100%" }}
                ></ImgCustom>
              </ImgWrapper>
              <Media heading>How Technology Drive Interior Design?</Media>
              <p>
                Designing the interior of a room has been at play since the
                prehistoric cultural age. Since then, it has seen developments
                in style, functionality, and sustainability. For now, designing
                the interior of a room or building is done with help from
                several technological advancements. We will explore the state of
                the technological interior design industry and where it is
                heading.
              </p>
              <p>
                The industry of interior design seems to have been somewhat of a
                late bloomer when it comes to incorporating technology into its
                processes. Despite this, the industry has finally caught up and
                is fully immersed in using technology to enhance the space in
                which we exist. When it comes to technologizing the industry, a
                few recent trends have helped offset the use of digital and
                scientific measures in design.
              </p>
              <p>
                The movement towards sustainable living has had a profound
                impact on the materials and methods used to recreate internal
                spaces. Furthermore, our own living standards have generated
                high expectations of the constant availability of technology and
                how and when we use it.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image2}
                  alt="image_2"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Smart Homes</b>
              </p>
              <p>
                The role of the interior designer has expanded into turning
                traditional living spaces into high tech homes. For interior
                design, there is little that affects our view and perception of
                a space more than light. Twenty years ago, most control you
                could have over domestic lighting systems was a rotary dimmer.
                Today, people can control the lighting situation from their
                phone, set automated timers on bedroom lights, and even
                manipulate natural light by insisting the blinds open at certain
                times.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image3}
                  alt="image_3"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>3D Printing</b>
              </p>
              <p>
                The use of 3D printing in interior design has not only
                introduced new methods of creating one-of-a-kind, personalized
                furniture but has brought with it new and exciting ways of
                creating inexpensive prototypes quickly.
              </p>
              <p>
                It allows designers to experiment with new combinations of
                materials, shapes and structures that would otherwise be
                difficult with actual size objects that may not be instantly
                available.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image4}
                  alt="image_4"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Smart Design Tools</b>
              </p>
              <p>
                Technology has wrapped itself around almost everything we have
                come to know. The tools once used by interior designers, namely,
                tape measures, drawing paper, telephone communication methods,
                and level tools have all been highly technologized. Once the
                interior designer got her hands on them, they became central
                instruments in the industry. There once was a time when the only
                means of communicating with clients, suppliers, and prospects,
                was by making individual phone calls. Houzz, the online
                community, provides a digital space for architects, designers,
                and clients to engage and share ideas.
              </p>
              <p>
                Its Site Designer allows professional interior designers to
                publish their own work on its website. Whilst word of mouth and
                recommendations are a sure-fire way to find clients, this
                coverage and publication of a designer’s work is more likely to
                cast the net far and wide and attract new customers. To up the
                excitement, the Houzz 3d AR tool allows people to see and move
                over 500,000 products from the Houzz Marketplace in their home,
                before buying. These virtual visual aids have shaken the
                industry.
              </p>
              <p>
                <b>Suistanable Design</b>
              </p>
              <p>
                Sustainable development has become the greatest challenge of the
                21st Century. There is no exception for the interior design
                industry. In fact, the industry relies heavily on sustainable
                development. One of the most important elements used to enhance
                the interior space in which we live in the air we breathe. It’s
                no surprise that sustainable design has taken off, and with some
                help by technology.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image5}
                  alt="image_5"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                Daylight design is well-documented to have health benefits.
                Before recent technological developments, the strategic placing
                of windows was the only tool available to the designer of a
                building. Today, energy-efficient light bulbs burn brighter, use
                less electricity, and last twice as long as a standard light
                bulb. Further, the introduction of Light Emitting Diodes (LED)
                have low heat generation, low power requirements, and are highly
                durable. Because their power inputs are minimal, LEDs are
                readily combined with solar panels to provide energy-efficient
                lighting day and night.
              </p>
              <ImgWrapper>
                <ImgCustom
                  src={Image6}
                  alt="image_6"
                  style={{ width: "30vw" }}
                ></ImgCustom>
              </ImgWrapper>
              <p>
                <b>Digital Furniture</b>
              </p>
              <p>
                Interior designers use furniture to establish an aesthetically
                pleasing and functional sense of order in a room. The changes in
                the way we interact with technology have changed the way we
                live, and hence use the space around us. Sitting on the couch to
                watch TV, and sitting on the couch to work on a laptop requires
                two different ways to use that couch. The modern furniture
                should be created to accommodate technology.
              </p>
              <p>
                That is not to merely suggest that furniture should include
                charging portals and interactive screens. Technology that is new
                today, will be replaced and obsolete by next year. Rather,
                furniture should be created with the new standard of living in
                mind. The Scene XXL chair by M2L, for example, comes with the
                option of an attached “tablet table,” and an upholstered high
                back for privacy when typing or making phone calls.
              </p>
              <p>
                From a design perspective, the computer and other mobile devices
                have gotten rid of so many things that were once used by
                designers to manipulate the ambiance of a room. Clocks are no
                longer a necessity because they are on our phones. File cabinets
                have become antiquated, as have records and in some cases books.
                In the past, designers had time to anticipate where tech was
                going and plan for it. Today, space and furniture have to be
                constantly aware of it.
              </p>
            </Media>
          </Media>
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default ArticleGeneral1;
