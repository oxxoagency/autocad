import React from "react";

import {
  Container,
  Row,
  Col,
  Card,
  Button,
  CardImg,
  CardTitle,
  CardBody
  // Pagination,
  // PaginationItem,
  // PaginationLink
} from "reactstrap";

import BackToTop from "react-back-to-top-button";

import styled from "styled-components";

import Image1 from "../../../assets/img/content_card_general_1.png";
import Image2 from "../../../assets/img/content_card_general_2.png";
import Image3 from "../../../assets/img/content_card_general_3.png";
import Image4 from "../../../assets/img/content_card_general_4.png";
import Image5 from "../../../assets/img/content_card_general_5.png";

const ContainerCustom = styled(Container)`
  padding: 0px !important;
`;

const CardCustom = styled(Card)`
  @media screen and (min-width: 601px) {
    padding: 50px;
    overflow: auto;
    border: none;
  }

  @media screen and (max-width: 600px) {
    padding: 10px;
    overflow: auto;
    border: none;
  }
`;

const CardArticleCustom = styled(Card)`
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
`;

const GeneralArticles = props => {
  return (
    <React.Fragment>
      <ContainerCustom fluid>
        <CardCustom>
          <Row>
            <Col sm="4">
              <CardArticleCustom>
                <CardImg
                  top
                  width="100%"
                  src={Image1}
                  alt="Card image cap"
                  style={{ padding: "20px" }}
                />
                <CardBody>
                  <CardTitle style={{ fontWeight: "600" }}>
                    How Technology Drive Interior Design
                  </CardTitle>
                  <Button
                    color="link"
                    href="/articles/general/1/1"
                    style={{
                      boxShadow: "none",
                      color: "red",
                      padding: "0",
                      marginTop: "20px"
                    }}
                  >
                    Selengkapnya
                  </Button>
                </CardBody>
              </CardArticleCustom>
            </Col>
            <Col sm="4">
              <CardArticleCustom>
                <CardImg
                  top
                  width="100%"
                  src={Image2}
                  alt="Card image cap"
                  style={{ padding: "20px" }}
                />
                <CardBody>
                  <CardTitle style={{ fontWeight: "600" }}>
                    Technology Developments and Easier Room Design
                  </CardTitle>
                  <Button
                    color="link"
                    href="/articles/general/1/2"
                    style={{
                      boxShadow: "none",
                      color: "red",
                      padding: "0",
                      marginTop: "20px"
                    }}
                  >
                    Selengkapnya
                  </Button>
                </CardBody>
              </CardArticleCustom>
            </Col>
            <Col sm="4">
              <CardArticleCustom>
                <CardImg
                  top
                  width="100%"
                  src={Image3}
                  alt="Card image cap"
                  style={{ padding: "20px" }}
                />
                <CardBody>
                  <CardTitle style={{ fontWeight: "600" }}>
                    Easy Designing on The Go
                  </CardTitle>
                  <Button
                    color="link"
                    href="/articles/general/1/3"
                    style={{
                      boxShadow: "none",
                      color: "red",
                      padding: "0",
                      marginTop: "20px"
                    }}
                  >
                    Selengkapnya
                  </Button>
                </CardBody>
              </CardArticleCustom>
            </Col>
          </Row>
          <Row>
            <Col sm="4">
              <CardArticleCustom>
                <CardImg
                  top
                  width="100%"
                  src={Image4}
                  alt="Card image cap"
                  style={{ padding: "20px" }}
                />
                <CardBody>
                  <CardTitle style={{ fontWeight: "600" }}>
                    Get to know Revit : Special App on Architecture
                  </CardTitle>
                  <Button
                    color="link"
                    href="/articles/general/1/4"
                    style={{
                      boxShadow: "none",
                      color: "red",
                      padding: "0",
                      marginTop: "20px"
                    }}
                  >
                    Selengkapnya
                  </Button>
                </CardBody>
              </CardArticleCustom>
            </Col>
            <Col sm="4">
              <CardArticleCustom>
                <CardImg
                  top
                  width="100%"
                  src={Image5}
                  alt="Card image cap"
                  style={{ padding: "20px" }}
                />
                <CardBody>
                  <CardTitle style={{ fontWeight: "600" }}>
                    AutoCAD More Value Compared with Other Software
                  </CardTitle>
                  <Button
                    color="link"
                    href="/articles/general/1/5"
                    style={{
                      boxShadow: "none",
                      color: "red",
                      padding: "0",
                      marginTop: "20px"
                    }}
                  >
                    Selengkapnya
                  </Button>
                </CardBody>
              </CardArticleCustom>
            </Col>
            {/* <Col sm="4">
              <Card>
                <CardImg
                  top
                  width="100%"
                  src="/assets/318x180.svg"
                  alt="Card image cap"
                />
                <CardBody>
                  <CardTitle>Card title</CardTitle>
                  <Button>Button</Button>
                </CardBody>
              </Card>
            </Col> */}
          </Row>

          {/* hide for temporary condition */}
          {/* <Pagination
            aria-label="Page navigation"
            style={{ float: "right", marginTop: "20px" }}
          >
            <PaginationItem>
              <PaginationLink first href="#" />
            </PaginationItem>
            <PaginationItem>
              <PaginationLink previous href="#" />
            </PaginationItem>
            <PaginationItem active>
              <PaginationLink href="#">1</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink href="#">2</PaginationLink>
            </PaginationItem>
            <PaginationItem>
              <PaginationLink next href="#" />
            </PaginationItem>
            <PaginationItem>
              <PaginationLink last href="#" />
            </PaginationItem>
          </Pagination> */}
        </CardCustom>
        <BackToTop
          showOnScrollUp={false}
          showAt={100}
          speed={1500}
          easing="easeInOutQuint"
        >
          <Button color="danger">
            <i className="cui-arrow-top"></i>
          </Button>
        </BackToTop>
      </ContainerCustom>
    </React.Fragment>
  );
};

export default GeneralArticles;
