import React from "react";
import {
  Container,
  Card,
  CardImg,
  CardTitle,
  CardText,
  CardDeck,
  CardSubtitle,
  CardBody,
} from "reactstrap";

import styled from "styled-components";

import SampleImage1 from "../../../assets/img/sample_event_1.png";
import SampleImage2 from "../../../assets/img/sample_event_2.png";
import SampleImage3 from "../../../assets/img/sample_event_3.png";

const ContainerCustom = styled(Container)`
  background: white;
  padding: 80px !important;
  margin-left: 0;
`;

const CustomText = styled.h3`
  @media screen and (max-width: 600px) {
    font-size: 15px;
  }
`;

const CardCustom = styled(Card)`
  border: none;
`;

const CardContent = () => {
  return (
    <ContainerCustom fluid>
      <div
        className="clearfix"
        style={{ padding: ".5rem", marginBottom: "40px" }}
      >
        <CustomText className="float-left">Kegiatan</CustomText>
        <a href="/events" className="float-right">
          <CustomText style={{ color: "red" }}>Lihat Semua</CustomText>
        </a>
      </div>
      <CardDeck>
        <CardCustom>
          <CardImg
            top
            width="100%"
            src={SampleImage1}
            alt="Card image cap"
            style={{ height: "60%" }}
          />
          <CardBody>
            <CardTitle style={{ fontSize: "20px" }}>Teknologi Pintar</CardTitle>
            <CardSubtitle>10 Juli 2020</CardSubtitle>
            <CardText style={{ marginTop: "15px", color: "#8E8E8E" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et
              vivamus mi duis mauris, metus morbi feugiat.{" "}
            </CardText>
          </CardBody>
        </CardCustom>
        <CardCustom>
          <CardImg
            top
            width="100%"
            src={SampleImage2}
            alt="Card image cap"
            style={{ height: "60%" }}
          />
          <CardBody>
            <CardTitle style={{ fontSize: "20px" }}>
              Webinar 3D Design
            </CardTitle>
            <CardSubtitle>13 Juli 2020</CardSubtitle>
            <CardText style={{ marginTop: "15px", color: "#8E8E8E" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et
              vivamus mi duis mauris, metus morbi feugiat.{" "}
            </CardText>
          </CardBody>
        </CardCustom>
        <CardCustom>
          <CardImg
            top
            width="100%"
            src={SampleImage3}
            alt="Card image cap"
            style={{ height: "60%" }}
          />
          <CardBody>
            <CardTitle style={{ fontSize: "20px" }}>
              Infrastruktur Design
            </CardTitle>
            <CardSubtitle>20 Juli 2020</CardSubtitle>
            <CardText style={{ marginTop: "15px", color: "#8E8E8E" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et
              vivamus mi duis mauris, metus morbi feugiat.{" "}
            </CardText>
          </CardBody>
        </CardCustom>
      </CardDeck>
    </ContainerCustom>
  );
};

export default CardContent;
