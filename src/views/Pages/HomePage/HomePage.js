import React, { Component } from "react";

import styled from "styled-components";

import Header from "./../../../containers/Header";

// import BackgroundLandingPage from "../../../assets/img/homepage_asset1.jpg";
import BackgroundLandingPage from "../../../assets/img/homepage_asset2.jpg";

import FirstContainer from "./FirstContainer";
// import SecondContainer from "./SecondContainer";
import Footer from "./../../../containers/Footer";

const Layout = styled.div`
  background: white;
`;

const Body = styled.div`
  width: 100%;
  height: auto;
  background: url(${BackgroundLandingPage}) no-repeat center fixed;
  backgorund: white;
  background-size: 100% 100%;
  margin-top: 0;
`;
class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      location: this.props.location.pathname
    };
  }

  render() {
    const { location } = this.state;

    return (
      <Layout className="app">
        <Body className="app-body animated fadeIn">
          <main className="main">
            <Header location={location} />
            <FirstContainer history={this.props.history} />
            {/* <SecondContainer /> */}
          </main>
        </Body>
        <Footer />
      </Layout>
    );
  }
}

export default HomePage;
