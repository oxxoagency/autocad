import React, { useState } from "react";

import ChatBot from "react-simple-chatbot";

import {
  Container,
  Jumbotron,
  Card,
  CardBody,
  CardImg,
  CardTitle,
  CardGroup,
  Row,
  Col,
  Carousel,
  CarouselItem,
  // CarouselCaption,
  CarouselIndicators,
  CarouselControl
  // Modal,
  // ModalHeader,
  // ModalBody
} from "reactstrap";

// import ReactTypingEffect from "react-typing-effect";

import styled, { ThemeProvider } from "styled-components";

import SampleImagePromoCarousel from "../../../assets/img/Carousel_1.jpg";
import SampleImageArticleCarousel from "../../../assets/img/Carousel_2.jpg";
import SampleImageProductCarousel from "../../../assets/img/Carousel_3.jpg";
import SampleImageInfo1 from "../../../assets/img/info_1.jpg";
import SampleImageInfo2 from "../../../assets/img/info_2.jpg";
import SampleImageInfo3 from "../../../assets/img/info_3.jpg";
import SampleImageInfo4 from "../../../assets/img/info_4.jpg";

const JumbotronCustom = styled(Jumbotron)`
  @media screen and (min-width: 1901px) {
    background: transparent;
    margin-bottom: 0;
    width: 100%;
    padding: 60px;
  }

  @media screen and (max-width: 1900px) and (min-width: 375px) {
    background: white;
  }
`;

const TagLine = styled.div`
  @media screen and (min-width: 1900px) {
    margin-bottom: 10px;
    font-size: 1.5rem;
    font-weight: 600;
    color: white;
    background: linear-gradient(135deg, #ea322a 40%, #ac2222 60%);
    border-radius: 10px;
    text-align: center;
    padding: 30px;
  }

  @media screen and (min-width: 300px) and (max-width: 1899px) {
    margin-bottom: 10px;
    font-size: 1rem;
    font-weight: 600;
    color: white;
    background: linear-gradient(135deg, #ea322a 40%, #ac2222 60%);
    border-radius: 10px;
    text-align: center;
    padding: 30px;
  }
`;

const CarouselWrapper = styled.div`
  @media screen and (min-width: 1901px) {
    width: 100%;
  }

  @media screen and (max-width: 1900px) and (min-width: 375px) {
    width: 100%;
  }
`;

const theme = {
  headerBgColor: "red",
  headerFontColor: "#fff",
  headerFontSize: "15px",
  botBubbleColor: "red",
  botFontColor: "#fff",
  userBubbleColor: "#F9F9F9"
};

// const Text =
//   "Telah dipercaya 10 tahun sebagai distributor Autodesk di Indonesia.";

const CardImgCustom = styled(CardImg)`
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
  cursor: pointer;

  @media screen and (min-width: 601px) {
    height: 28vh;
  }

  @media screen and (max-width: 600px) {
    height: 20vh;
  }
`;

const FirstContainer = props => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);
  const [opened, setOpened] = useState(false);

  const redirectToArticlesPage1 = () => {
    props.history.push("/products/autocad_including_specialized_toolset_2021");
  };

  const redirectToArticlesPage2 = () => {
    props.history.push("/articles/transition_to_name_user");
  };

  const redirectToArticlesPage3 = () => {
    props.history.push("/articles/new_features_2021");
  };

  // const redirectToProductsPage = () => {
  //   props.history.push(
  //     "/products/autocad_including_specialized_toolset_2021/architecture_toolset"
  //   );
  // };

  // const redirectToEventsPage = () => {
  //   props.history.push("/events");
  // };

  const redirectToSupportPage = () => {
    props.history.push("/support");
  };

  const redirectToWhatsApp = () => {
    window.open("https://wa.me/6287870857405", "_blank");
  };

  const items = [
    {
      id: "1",
      src: SampleImagePromoCarousel,
      altText: "Promo",
      caption: "Promo",
      style: {
        padding: "20px",
        border: "2px solid #f9f9f9",
        borderRadius: "10px",
        cursor: "pointer"
      }
      // onClick: redirectToProductsPage
    },
    {
      id: "2",
      src: SampleImageArticleCarousel,
      altText: "Artikel",
      caption: "Artikel",
      style: {
        padding: "20px",
        border: "2px solid #f9f9f9",
        borderRadius: "10px",
        cursor: "pointer"
      }
      // onClick: redirectToArticlesPage
    },
    {
      id: "3",
      src: SampleImageProductCarousel,
      altText: "Produk",
      caption: "Produk",
      style: {
        padding: "20px",
        border: "2px solid #f9f9f9",
        borderRadius: "10px",
        cursor: "pointer"
      }
      // onClick: redirectToEventsPage
    }
  ];

  const steps = [
    {
      id: "1",
      message: "Halo, Boleh Aku Tau Nama Kamu?",
      trigger: "2"
    },
    {
      id: "2",
      user: true,
      trigger: "3"
    },
    {
      id: "3",
      message: "Hi {previousValue}, Senang Bisa Berkenalan Denganmu",
      trigger: "4"
    },
    {
      id: "4",
      message: "Kategori bantuan seperti apa yang bisa saya bantu?",
      trigger: "5"
    },
    {
      id: "5",
      options: [
        { value: 1, label: "Standard", trigger: "6" },
        { value: 2, label: "Urgent", trigger: "7" },
        { value: 3, label: "Critical", trigger: "8" }
      ]
    },
    {
      id: "6",
      message: "Aku akan mengarahkanmu ke halaman support",
      trigger: "9"
    },
    {
      id: "7",
      message: "Aku akan mengarahkanmu ke halaman support",
      trigger: "9"
    },
    {
      id: "8",
      message: "Aku akan mengarahkanmu ke nomor teknikal support ya",
      trigger: "10"
    },
    {
      id: "9",
      message: "OK",
      trigger: redirectToSupportPage
    },
    {
      id: "10",
      message: "OK",
      trigger: redirectToWhatsApp
    }
  ];

  const toggleFloating = () => {
    setOpened(!opened);
  };

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  const goToIndex = newIndex => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  const slides = items.map(item => {
    return (
      <CarouselItem
        className="custom-tag"
        tag="div"
        key={item.id}
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
      >
        <img
          src={item.src}
          alt={item.altText}
          style={item.style}
          onClick={item.onClick}
        />
        {/* <CarouselCaption
          captionText={item.caption}
          captionHeader={item.caption}
        /> */}
      </CarouselItem>
    );
  });

  // const [modal, setModal] = useState(false);

  // const toggleModal = () => setModal(!modal);

  return (
    <React.Fragment>
      <Container fluid>
        <JumbotronCustom>
          <CarouselWrapper>
            <CardGroup>
              <Card style={{ background: "transparent", border: "none" }}>
                <CardBody>
                  <TagLine>
                    {/* <ReactTypingEffect
                      text={Text}
                      typingDelay={500}
                      eraseDelay={1000}
                    /> */}
                    Telah dipercaya 10 tahun sebagai distributor Autodesk di
                    Indonesia
                  </TagLine>
                  <div>
                    <style>
                      {`@media screen and (max-width: 600px) {
                        .custom-tag  > img {
                            width: 100%;
                            height: auto;
                          }
                        }
                        @media screen and (min-width: 601px) {
                          .custom-tag  > img {
                              width: 100%;
                              height: 54vh;
                            }
                          }
                        @media screen and (max-width: 600px) {
                          .carousel-indicators li {
                              height: 3px;
                              background-color: red;
                          }
                        }
                        @media screen and (min-width: 601px) {
                          .carousel-indicators li {
                              height: 15px;
                              background-color: red;
                          }
                        }
                      `}
                      {/* {`.rsc-container {
                           background: white;
                           border-radius: 0;
                           bottom: 0 !important;
                           left: initial !important;
                           height: 100%;
                           right: 0 !important;
                           top: initial !important;
                           width: 100%;
                      }`}
                      {`.rsc-content {
                          height: calc(100% - 112px);
                      }`} */}
                    </style>
                    <Carousel
                      activeIndex={activeIndex}
                      next={next}
                      previous={previous}
                      interval={3000}
                      ride="carousel"
                    >
                      <CarouselIndicators
                        items={items}
                        activeIndex={activeIndex}
                        onClickHandler={goToIndex}
                      />
                      {slides}
                      <CarouselControl
                        direction="prev"
                        directionText="Previous"
                        onClickHandler={previous}
                      />
                      <CarouselControl
                        direction="next"
                        directionText="Next"
                        onClickHandler={next}
                      />
                    </Carousel>
                  </div>
                </CardBody>
              </Card>
              {/* <Modal isOpen={modal} toggle={toggleModal} centered>
                <ModalHeader toggle={toggleModal}>Modal title</ModalHeader>
                <ModalBody>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                  irure dolor in reprehenderit in voluptate velit esse cillum
                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                  cupidatat non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.
                </ModalBody>
              </Modal> */}
              <Card style={{ background: "transparent", border: "none" }}>
                <CardBody>
                  <CardTitle style={{ fontSize: "1.5rem", fontWeight: "600" }}>
                    Info Terkini
                  </CardTitle>
                  <Row style={{ marginBottom: "20px" }}>
                    <Col sm="6" style={{ marginBottom: "20px" }}>
                      <div class="hvr-grow">
                        <div class="ver_mas text-center">
                          <span onClick={redirectToArticlesPage3}>
                            Selengkapnya
                          </span>
                        </div>
                        <CardImgCustom
                          width="100%"
                          src={SampleImageInfo1}
                          alt="Card image cap"
                          // onClick={toggleModal}
                        />
                      </div>
                    </Col>
                    <Col sm="6">
                      <div class="hvr-grow">
                        <div class="ver_mas text-center">
                          <span onClick={redirectToArticlesPage3}>
                            Selengkapnya
                          </span>
                        </div>
                        <CardImgCustom
                          width="100%"
                          src={SampleImageInfo2}
                          alt="Card image cap"
                          // onClick={toggleModal}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col sm="6" style={{ marginBottom: "20px" }}>
                      <div class="hvr-grow">
                        <div class="ver_mas text-center">
                          <span onClick={redirectToArticlesPage1}>
                            Selengkapnya
                          </span>
                        </div>
                        <CardImgCustom
                          width="100%"
                          src={SampleImageInfo3}
                          alt="Card image cap"
                          // onClick={toggleModal}
                        />
                      </div>
                    </Col>
                    <Col sm="6">
                      <div class="hvr-grow">
                        <div class="ver_mas text-center">
                          <span onClick={redirectToArticlesPage2}>
                            Selengkapnya
                          </span>
                        </div>
                        <CardImgCustom
                          width="100%"
                          src={SampleImageInfo4}
                          alt="Card image cap"
                          // onClick={toggleModal}
                        />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </CardGroup>
          </CarouselWrapper>
        </JumbotronCustom>
        <ThemeProvider theme={theme}>
          <ChatBot
            headerTitle="Pesan"
            steps={steps}
            floating={true}
            opened={opened}
            toggleFloating={toggleFloating}
            placeholder="Ketik Pesanmu Disini..."
            style={{ background: "white" }}
          />
        </ThemeProvider>
      </Container>
    </React.Fragment>
  );
};

export default FirstContainer;
