import React, { Component } from "react";

import axios from "axios";

import {
  Button,
  CardGroup,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import { Link } from "react-router-dom";

import RightContent from "../../../assets/img/Schedule_AutoCAD.jpg";
import Logo from "../../../assets/img/autocad_logo.jpg";
import { CircleToBlockLoading } from "react-loadingg";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      user_hp: "",
      user_fullname: "",
      user_company: "",
      job_id: 1,
      job_user: [
        { status: 0, user_job_id: 1, user_job_name: "Pilih jabatan anda" },
        { status: 0, user_job_id: 2, user_job_name: "Drafter" },
        { status: 0, user_job_id: 3, user_job_name: "Engineering Manager" },
        { status: 0, user_job_id: 4, user_job_name: "Project Manager" },
        { status: 0, user_job_id: 5, user_job_name: "Production Engineer" },
        {
          status: 0,
          user_job_id: 6,
          user_job_name: "Mechanical Designer Engineer",
        },
        {
          status: 0,
          user_job_id: 7,
          user_job_name: "Electrical Designer Engineer",
        },
        { status: 0, user_job_id: 8, user_job_name: "Structural Engineer" },
        { status: 0, user_job_id: 9, user_job_name: "Civil Engineer" },
        { status: 0, user_job_id: 10, user_job_name: "MEP Engineer" },
        { status: 0, user_job_id: 11, user_job_name: "Urban Planner" },
        { status: 0, user_job_id: 12, user_job_name: "GIS Professional" },
        { status: 0, user_job_id: 13, user_job_name: "Mahasiswa" },
        { status: 0, user_job_id: 14, user_job_name: "Pekerjaan lainnya" },
      ],
      job_others: "",
      user_company_address: "",
      user_company_phone: "",
      user_materi_options: [
        {
          name: "Materi apa yang anda minati pada acara AutoCAD DAY ? ",
          value: 0,
        },
        {
          name: "AutoCAD for AEC (Architecture, Engineering & Construction)",
          value: 1,
        },
        { name: "AutoCAD for Manufacturing", value: 2 },
      ],
      user_materi: 0,
      loader: false,
    };
  }

  componentDidMount() {
    // set Auth TOKEN to local storage
    // env prod
    const url = `https://cors-anywhere.herokuapp.com/http://175.41.160.70:8181/oauth/token`;
    // const url = `http://175.41.160.70:8181/oauth/token`;

    const credentials = {
      username: "autocad@mail.com",
      password: "rahasia",
      grant_type: "password",
      client_id: "2",
      client_secret: "WABXKLbVCY6de4KV7r2HogD0ALNfCWs34jxX5BaU",
      provider: "userdata",
    };

    const headers = {
      "Content-Type": "application/json",
    };

    axios
      .post(url, credentials, {
        headers: headers,
      })
      .then((res) => {
        window.localStorage.setItem("authToken", res.data.access_token);
      });
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({ loader: true });

    const {
      email,
      user_hp,
      user_fullname,
      user_company,
      job_id,
      job_others,
      user_company_address,
      user_company_phone,
      user_materi,
    } = this.state;
    const authToken = window.localStorage.getItem("authToken");

    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${authToken}`,
    };

    // env prod
    // const url = `https://cors-anywhere.herokuapp.com/http://autocadelbas.oxxo.co.id:8181//register`;
    const url = `https://autocadelbas.oxxo.co.id/register`;

    axios
      .post(
        url,
        {
          email,
          user_hp,
          user_fullname,
          user_company,
          job_id,
          job_others,
          user_company_address,
          user_company_phone,
          user_materi,
        },
        {
          headers: headers,
        }
      )
      .then((res) => {
        this.setState({ loader: false });
        if (
          res.data.status === 200 &&
          email !== "" &&
          job_id !== 1 &&
          user_materi !== 0
        ) {
          alert("Thanks for Register");
          this.props.history.push("/success");
        } else if (
          res.data.status === 404 &&
          email !== "" &&
          res.data.message.email !== undefined
        ) {
          // Error from server;
          alert("Email Sudah Terdaftar");
        } else if (res.data.status === 404 && email !== "") {
          alert("Mohon isi lengkap form registrasi");
        } else {
          alert("Harap lengkapi data yang dibutuhkan untuk registrasi");
        }
      });
  };

  render() {
    const {
      email,
      user_hp,
      user_fullname,
      user_company,
      job_id,
      job_user,
      job_others,
      user_company_address,
      user_company_phone,
      user_materi_options,
      user_materi,
    } = this.state;
    if (this.state.loader) {
      return <CircleToBlockLoading color="#20a8d8" />;
    } else {
      return (
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="12">
                <CardGroup>
                  <Card>
                    <CardBody className="p-4">
                      <Form>
                        <Row>
                          <Col md="8">
                            <h1>Register</h1>
                          </Col>
                          <img
                            src={Logo}
                            style={{ width: "150px", height: "40px" }}
                            alt="logo"
                          />
                        </Row>
                        <p className="text-muted">
                          Syarat & Ketentuan :
                          <br />
                          1. Silakan lengkapi data berikut untuk pendaftaran
                          <br />
                          2. Semua kolom wajib di isi
                          <br />
                          3. Tolong gunakan email yang valid untuk keperluan
                          verifikasi
                        </p>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="user_fullname"
                            type="text"
                            placeholder="Fullname"
                            autoComplete="fullname"
                            required
                            value={user_fullname}
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>@</InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="email"
                            type="email"
                            placeholder="Email"
                            autoComplete="email"
                            required
                            value={email}
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-home"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="user_company"
                            type="text"
                            placeholder="Company/Agency/Campus Name"
                            autoComplete="user_company"
                            required
                            value={user_company}
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-briefcase"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="job_id"
                            type="select"
                            value={job_id}
                            onChange={this.handleChange}
                          >
                            {job_user.map((item) => (
                              <option
                                key={item.user_job_id}
                                value={item.user_job_id}
                              >
                                {item.user_job_name}
                              </option>
                            ))}
                          </Input>
                        </InputGroup>
                        {job_id === "14" ? (
                          <InputGroup className="mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="icon-briefcase"></i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              name="job_others"
                              type="text"
                              placeholder="Silakan masukkan jenis pekerjaan lainnya"
                              value={job_others}
                              onChange={this.handleChange}
                            />
                          </InputGroup>
                        ) : (
                          ""
                        )}
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-home"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="user_company_address"
                            type="textarea"
                            placeholder="Company/Agency/Campus Address"
                            autoComplete="user_company_address"
                            required
                            value={user_company_address}
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-phone"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="user_company_phone"
                            type="text"
                            placeholder="Company/Agency/Campus Phone Number"
                            autoComplete="user_company_phone_number"
                            value={user_company_phone}
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-phone"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="user_hp"
                            type="text"
                            placeholder="Your Phone Number"
                            autoComplete="user_phone_number"
                            required
                            value={user_hp}
                            onChange={this.handleChange}
                          />
                        </InputGroup>
                        <InputGroup className="mb-4">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-key"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            name="user_materi"
                            type="select"
                            value={user_materi}
                            onChange={this.handleChange}
                          >
                            {user_materi_options.map((item) => (
                              <option key={item.value} value={item.value}>
                                {item.name}
                              </option>
                            ))}
                          </Input>
                        </InputGroup>
                        <Link to="/lobby">
                          <Button
                            type="submit"
                            color="primary"
                            onClick={this.handleSubmit}
                            block
                          >
                            Sign Up
                          </Button>
                        </Link>
                        <div style={{ marginTop: "10px" }}></div>
                        <Link to="/">
                          <Button color="danger" block>
                            Back to Login Page
                          </Button>
                        </Link>
                      </Form>
                    </CardBody>
                  </Card>
                  <Card>
                    <CardBody className="text-center">
                      <div>
                        <img
                          src={RightContent}
                          className="d-md-down-none"
                          style={{ height: "700px" }}
                          alt="schedule"
                        />
                      </div>
                    </CardBody>
                  </Card>
                </CardGroup>
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
  }
}

export default Register;
