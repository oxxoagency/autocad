import React from "react";

import { Nav, NavItem, NavLink } from "reactstrap";

import styled from "styled-components";

const NavLinkCustom = styled(NavLink)`
  color: black;
  :hover {
    color: red;
  }
`;

const LineBreakSideBar = styled.hr`
  width: 100px;
  float: left;
  margin-left: 20px;
`;

const SideNavEventsPage = props => {
  return (
    <React.Fragment>
      <style>
        {`.active  > .nav-link {
              color: red;
            }`}
      </style>
      <Nav vertical style={{ padding: "50px" }}>
        <NavItem>
          <NavLinkCustom href="#">AutoCAD DAY</NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem>
          <NavLinkCustom href="#">Training Product</NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem>
          <NavLinkCustom href="#">Webinar</NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
        <NavItem>
          <NavLinkCustom href="#">Workshop AutoCAD</NavLinkCustom>
          <LineBreakSideBar />
        </NavItem>
      </Nav>
    </React.Fragment>
  );
};

export default SideNavEventsPage;
