import React from "react";

import styled from "styled-components";

import {
  Row //Col
} from "reactstrap";
import Header from "./../../../containers/Header";

// import SideNav from "./SideNav";

import FirstContainer from "./FirstContainer";

import Footer from "./../../../containers/Footer";

const Layout = styled.div`
  background: white;
`;

const Body = styled.div`
  width: 100%;
  height: auto;
  background: white;
  margin-top: 0;
`;

const RowCustom = styled(Row)`
  @media screen and (min-width: 601px) {
    padding: 40px;
  }

  @media screen and (max-width: 600px) {
    padding: 0px;
  }
`;

const EventsPage = props => {
  const location = props.location.pathname;
  const history = props.history;

  return (
    <Layout className="app">
      <Body className="app-body animated fadeIn">
        <main className="main">
          <Header location={location} />
          <RowCustom>
            {/* <Col md="2">
              <SideNav location={location} />
            </Col> */}
            {/* <Col xl="9">{location === "/events" && <FirstContainer />}</Col> */}
            {location === "/events" && <FirstContainer history={history} />}
          </RowCustom>
        </main>
      </Body>
      <Footer />
    </Layout>
  );
};

export default EventsPage;
