// import React, { useState } from "react";
import React from "react";
import {
  Container,
  Card,
  CardImg,
  CardTitle,
  CardText,
  // CardSubtitle,
  CardBody,
  Button
  // Modal,
  // ModalHeader,
  // ModalBody
} from "reactstrap";

import styled from "styled-components";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

import AutoCADDay from "../../../assets/img/AutoCADDAY.jpg";
import TrainingProduct from "../../../assets/img/Training_Product.jpg";
import Webinar from "../../../assets/img/Webinar.jpg";
import Workshop from "../../../assets/img/Workshop.jpg";

const ContainerCustom = styled(Container)`
  @media screen and (min-width: 601px) {
    background: white;
    padding-right: 80px !important;
    padding-left: 80px !important;
    margin-left: 0;
  }

  @media screen and (max-width: 600px) {
    background: white;
    padding-right: 0px !important;
    padding-left: 0px !important;
    margin-left: 0;
  }
`;

const LinkCustom = styled(Button)`
  padding: 0;
  color: red;
  :focus {
    box-shadow: none;
  }
`;

const CardCustom = styled(Card)`
  border: none;
  margin: 50px;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
`;

const EventsPageContent = props => {
  // const [modal, setModal] = useState(false);

  // const toggleModal = () => setModal(!modal);

  const redirectToSupportPage = () => {
    props.history.push("/support");
  };

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1
    }
  };

  return (
    <ContainerCustom fluid>
      <style>
        {`.react-multiple-carousel__arrow {
            background: red;
          }
          .react-multiple-carousel__arrow:hover {
            background: #0d92d9;
          }
        `}
      </style>
      {/* <Modal isOpen={modal} toggle={toggleModal} centered>
        <ModalHeader toggle={toggleModal}>Modal title</ModalHeader>
        <ModalBody>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </ModalBody>
      </Modal> */}
      <div>
        <Carousel responsive={responsive}>
          <div>
            <CardCustom>
              <CardImg
                top
                width="100%"
                src={AutoCADDay}
                alt="Card image cap"
                // style={{ height: "60%" }}
              />
              <CardBody>
                <CardTitle style={{ fontSize: "20px" }}>AutoCAD DAY</CardTitle>
                {/* <CardSubtitle>10 Juli 2020</CardSubtitle> */}
                <CardText style={{ marginTop: "15px", color: "#8E8E8E" }}>
                  AutoCAD DAY merupakan Event besar kami bekerjasama dengan
                  autodesk Indonesia untuk menyelenggarakan sebuah acara besar
                  dalam rangka memberikan informasi terbaru tentang autodesk,
                  memperkenalkan produk autodesk, dan memberikan solusi terbaik
                  untuk sebuah pekerjaan desain baik di bidang manufaktur
                  ataupun di bidang Arsitek, sipil, dan konstruksi. Acara ini
                  diselenggarakan di kota-kota besar Indonesia dengan pembicara
                  langsung dari autodesk Indonesia dan Singapura.{" "}
                </CardText>
                {/* <LinkCustom color="link" onClick={toggleModal}>
                  Lihat Kegiatan
                </LinkCustom> */}
              </CardBody>
            </CardCustom>
          </div>
          <div>
            <CardCustom>
              <CardImg
                top
                width="100%"
                src={TrainingProduct}
                alt="Card image cap"
              />
              <CardBody>
                <CardTitle style={{ fontSize: "20px" }}>
                  Training Product
                </CardTitle>
                {/* <CardSubtitle>13 Juli 2020</CardSubtitle> */}
                <CardText style={{ marginTop: "15px", color: "#8E8E8E" }}>
                  Kami telah dipercaya selama 10 tahun sebagai distributor
                  autodesk di Indoesia, tidak hanya melayani software autodesk
                  tetapi kami juga melayani training software autodesk.
                  Bekerjasama dengan beberapa pusat pelatihan resmi autodesk
                  (Authorized Training Centers) yang ada di Indonesia serta para
                  pelatih yang sudah bersertifikat resmi dari autodesk. Kami
                  melayani kebutuhan training sebagai berikut :
                  <br />
                  - AutoCAD 2D dan atau 3D - AutoCAD Architecture
                  <br />
                  - AutoCAD MEP
                  <br />
                  - AutoCAD Mechanical
                  <br />
                  - AutoCAD Electrical
                  <br />
                  - AutoCAD Plant 3D
                  <br />
                  - AutoCAD Raster Design
                  <br />
                  - AutoCAD MAP 3D
                  <br />
                  - Product autodesk lainnya
                  <br />
                  Jika ada kebutuhan training autodesk silahkan menghubungi
                  kami.{" "}
                </CardText>
                <LinkCustom color="link" onClick={redirectToSupportPage}>
                  Hubungi Kami
                </LinkCustom>
              </CardBody>
            </CardCustom>
          </div>
          <div>
            <CardCustom>
              <CardImg top width="100%" src={Webinar} alt="Card image cap" />
              <CardBody>
                <CardTitle style={{ fontSize: "20px" }}>Webinar</CardTitle>
                {/* <CardSubtitle>20 Juli 2020</CardSubtitle> */}
                <CardText style={{ marginTop: "15px", color: "#8E8E8E" }}>
                  Selain melayani penjualan software autodesk kami juga
                  meyelenggarakan webinar untuk memberikan informasi terbaru
                  terkait licensinng autodesk, update produk autodesk, tip dan
                  trik menggunakan software autodesk, serta kami melayani
                  webinar sesuai request. Webinar ini bertujuan untuk membantu
                  anda lebih mengenali produk autodesk dan dapat lebih
                  meningkatkan pekerjaan anda dalam membuat sebuah design dengan
                  lebih baik lagi. Berikut daftar webinar yang akan berjalan
                  Terkait detail jadwal dan request webinar silahkan menghubungi
                  kami.{" "}
                </CardText>
                <LinkCustom color="link" onClick={redirectToSupportPage}>
                  Hubungi Kami
                </LinkCustom>
              </CardBody>
            </CardCustom>
          </div>
          <div>
            <CardCustom>
              <CardImg top width="100%" src={Workshop} alt="Card image cap" />
              <CardBody>
                <CardTitle style={{ fontSize: "20px" }}>Workshop</CardTitle>
                {/* <CardSubtitle>20 Juli 2020</CardSubtitle> */}
                <CardText style={{ marginTop: "15px", color: "#8E8E8E" }}>
                  Kami mengadakan workshop ke perusahaan-perusahaan yang ada di
                  seluruh Indonesia untuk memberikan informasi terbaru tentang
                  autodesk, dengan workshop ini kami bertujuan agar perusahaan
                  tersebut mampu menggunakan produk autodesk dengan maksimal dan
                  mampu meningkatkan produktifitas gambar kerja dengan lebih
                  baik lagi. Jika perusahaan anda ingin kami kunjungi untuk
                  mengadakan workshop silahkan menghubungi kami{" "}
                </CardText>
                <LinkCustom color="link" onClick={redirectToSupportPage}>
                  Hubungi Kami
                </LinkCustom>
              </CardBody>
            </CardCustom>
          </div>
        </Carousel>
      </div>
      <div style={{ marginTop: "20px" }}>{/* For Another List Events */}</div>
    </ContainerCustom>
  );
};

export default EventsPageContent;
